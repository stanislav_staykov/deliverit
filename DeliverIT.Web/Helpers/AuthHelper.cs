﻿using System.Security.Authentication;
using DeliverIT.Services.Contracts;

namespace AspNetCoreDemo.Web.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private static readonly string ErrorMessage = "Invalid authentication info.";

        private readonly IUserService userService;

        public AuthHelper(IUserService userService)
        {
            this.userService = userService;
        }

        public string TryGetUser(string email)
        {
            var user = this.userService.GetUserName(email);
            if(user == null)
            {
                throw new AuthenticationException(ErrorMessage);
            }

            return user;
        }

    }
}
