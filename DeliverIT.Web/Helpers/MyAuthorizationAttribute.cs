﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace AspNetCoreDemo.Web.Helpers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class MyAuthorizationAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var roles = context.HttpContext.Session.GetString("CurrentRoles").Split(',').ToList();

            if(!roles.Contains(this.Role))
            {
                context.Result = new UnauthorizedResult();
            }
        }

        public string Role { get; set; }
    }
}
