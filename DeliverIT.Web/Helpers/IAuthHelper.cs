﻿
namespace AspNetCoreDemo.Web.Helpers
{
    public interface IAuthHelper
    {
        public string TryGetUser(string email);

    }
}
