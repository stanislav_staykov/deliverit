using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreDemo.Web.Helpers;
using DeliverIT.Data;
using DeliverIT.Services;
using DeliverIT.Services.Contracts;
using DeliverIT.Web.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DeliverIT.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(1000);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            //services.AddRazorPages();
            services.AddDbContext<DeliverItContext>(options =>
            {
                options.UseSqlServer(@"Server=DESKTOP-3VF5E9T\SQLEXPRESS;Database=DeliverIt;Integrated Security=True");
            });
            services.AddControllersWithViews();
            services.AddScoped<ModelMapper>();
            services.AddScoped<ICRUDandFilterParcels, CRUDandFilterParcels>();
            services.AddScoped<ICRUDandFilterShipments, CRUDandFilterShipments>();
            services.AddScoped<ICheckingValues, CheckingValues>();
            services.AddScoped<IListAllCountriesCities, ListAllCountriesCities>();
            services.AddScoped<ICreateCities, CreateCities>();
            services.AddScoped<ICreateCountries, CreateCountries>();
            services.AddScoped<IListCreateModifyDeleteWarehouses, ListCreateModifyDeleteWarehouses>();
            services.AddScoped<ICreateAddresses, CreateAddresses>();
            services.AddScoped<ICRUDandSearchCustomer, CRUDandSearchCustomer>();
            services.AddScoped<IAuthHelper, AuthHelper>();
            services.AddScoped<IUserService, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if(env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //else
            //{
            //    app.UseExceptionHandler("/Error");
            //}

            app.UseStaticFiles();

            app.UseRouting();

            app.UseSession();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
