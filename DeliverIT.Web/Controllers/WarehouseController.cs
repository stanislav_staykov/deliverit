﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DeliverIT.Web.Controllers
{
    public class WarehouseController : Controller
    {
        private IListCreateModifyDeleteWarehouses listCreateModifyDeleteWarehouses;
        private ModelMapper modelMapper;

        public WarehouseController(IListCreateModifyDeleteWarehouses listCreateModifyDeleteWarehouses, ModelMapper modelMapper)
        {
            this.listCreateModifyDeleteWarehouses = listCreateModifyDeleteWarehouses;
            this.modelMapper = modelMapper;
        }

        public IActionResult Index()
        {
            var warehouses = this.listCreateModifyDeleteWarehouses.ListingWarehouse();

            return this.View(warehouses);
        }

        //GET: /warehouse/details/:id
        public IActionResult Details(int id)
        {
            try
            {
                var warehouse = this.listCreateModifyDeleteWarehouses.ListingWarehouseById(id);

                return this.View(warehouse);
            }
            catch(ArgumentException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                this.ViewData["error"] = $"Warehouse withid= {id} does not exist.";

                return this.View("Error");
            }
        }

        //GET: /warehouse/create
        public IActionResult Create()
        {
            if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            var warehouseViewModel = new WarehouseViewModel();

            return this.WarehouseView(warehouseViewModel);
        }

        //POST: /warehouse/create
        [HttpPost]
        public IActionResult Create([Bind("Warehouse,StreetName,City,Country")] WarehouseViewModel warehouseViewModel)
        {
            if(!this.ModelState.IsValid)
            {
                return this.WarehouseView(warehouseViewModel);
            }

            this.listCreateModifyDeleteWarehouses.Create(warehouseViewModel);

            return this.RedirectToAction(nameof(this.Index));
        }


        //GET: /warehouse/edit/:id
        public IActionResult Edit(int id)
        {
            try
            {
                if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
                {
                    return this.RedirectToAction("Login", "Auth");
                }

                var warehouse = this.listCreateModifyDeleteWarehouses.ListingWarehouseById(id);
                var parcelViewModel = this.modelMapper.ToWarehouseViewModel(warehouse);

                return this.WarehouseView(parcelViewModel);
            }
            catch(ArgumentException)
            {
                return this.ParcelNotFound(id);
            }
        }

        //POST: /warehouse/edit/:id
        [HttpPost]
        public IActionResult Edit(int id, [Bind("Id,Warehouse,NewWarehouseName")] WarehouseViewModel warehouseViewModel)
        {
            if(!ModelState.IsValid)
            {
                return this.WarehouseView(warehouseViewModel);
            }

            try
            {
                this.listCreateModifyDeleteWarehouses.Modify(warehouseViewModel.Warehouse, warehouseViewModel.NewWarehouseName);
            }
            catch(ArgumentException)
            {
                return this.ParcelNotFound(id);
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        private IActionResult ParcelNotFound(int id)
        {
            this.Response.StatusCode = StatusCodes.Status404NotFound;
            this.ViewData["error"] = $"Parcel with id {id} does not exist.";
            return this.View("Error");
        }

        private IActionResult WarehouseView(WarehouseViewModel warehouseViewModel)
        {
            warehouseViewModel.Addresses = this.GetAddresses();
            return this.View(warehouseViewModel);
        }

        private SelectList GetAddresses()
        {
            var categories = this.listCreateModifyDeleteWarehouses.GetAllAddresses();
            return new SelectList(categories, "Id", "Name");
        }
    }
}
