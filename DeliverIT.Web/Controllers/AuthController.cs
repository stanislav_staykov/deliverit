﻿using System;
using System.Linq;
using System.Security.Authentication;
using AspNetCoreDemo.Web.Helpers;
using AspNetCoreDemo.Web.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreDemo.Web.Controllers
{

    public class AuthController : Controller
    {
        private readonly IAuthHelper authHelper;
        private readonly IUserService userService;
        private readonly ModelMapper modelMapper;
        private readonly ICRUDandSearchCustomer cRUDandSearchCustomer;

        public AuthController(IAuthHelper authHelper, IUserService userService, ModelMapper modelMapper, ICRUDandSearchCustomer cRUDandSearchCustomer)
        {
            this.authHelper = authHelper;
            this.userService = userService;
            this.modelMapper = modelMapper;
            this.cRUDandSearchCustomer = cRUDandSearchCustomer;
        }

        //GET: /auth/login
        public IActionResult Login()
        {
            var loginViewModel = new LoginViewModel();

            return this.View(loginViewModel);
        }

        //POST: /auth/login
        [HttpPost]
        public IActionResult Login([Bind("Email")] LoginViewModel loginViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }

            try
            {
                var user = this.authHelper.TryGetUser(loginViewModel.Email);
                this.HttpContext.Session.SetString("CurrentUser", user);
                this.HttpContext.Session.SetString("CurrentRoles", this.userService.Get(loginViewModel.Email).ToString());

                return this.RedirectToAction("index", "home");
            }
            catch (AuthenticationException)
            {
                this.ModelState.AddModelError("Email", "Invalid Email address!");
                return this.View(loginViewModel);
            }
        }

        //GET: /auth/logout
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");

            return this.RedirectToAction("index", "home");
        }

        //GET: /auth/register
        public IActionResult Register()
        {
            var registerViewModel = new RegisterCustomerDTO();

            return this.View(registerViewModel);
        }

        [HttpPost]
        public IActionResult Register([Bind("FirstName ,LastName ,Email ,StreetName, City, Country")] RegisterCustomerDTO registerCustomerDTO)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(registerCustomerDTO);
            }

            try
            {
                var user = this.cRUDandSearchCustomer.SerchByEmail(registerCustomerDTO.Email);
                this.ModelState.AddModelError("Email", "User with same email already exists.");

                return this.View(registerCustomerDTO);
            }
            catch(ArgumentException)
            {
                this.cRUDandSearchCustomer.Create(registerCustomerDTO);
            }

            return this.RedirectToAction(nameof(this.Login));
        }
    }
}
