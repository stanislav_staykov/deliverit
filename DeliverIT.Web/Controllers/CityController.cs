﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DeliverIT.Web.Controllers
{
    public class CityController : Controller
    {
        private IListAllCountriesCities listAllCountriesCities;
        private ICreateCities createCities;
        private ModelMapper modelMapper;

        public CityController(IListAllCountriesCities listAllCountriesCities,ICreateCities createCities, ModelMapper modelMapper)
        {
            this.listAllCountriesCities = listAllCountriesCities;
            this.createCities = createCities;
            this.modelMapper = modelMapper;
        }

        public IActionResult Index()
        {
            var parcels = this.listAllCountriesCities.ListingCitiesForWeb();

            return this.View(parcels);
        }

        //GET: /city/details/:id
        public IActionResult Details(int id)
        {
            try
            {
                var city = this.listAllCountriesCities.GetCityById(id);
                var cityViewModel = this.modelMapper.ToCityModel(city, id);
                return this.View(cityViewModel);
            }
            catch(ArgumentException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                this.ViewData["error"] = $"Parcel withid= {id} does not exist.";

                return this.View("Error");
            }
        }

        //GET: /city/create
        public IActionResult Create()
        {
            if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            var city = new CitiesDTO();

            return this.CityView(city);
        }

        //POST: /city/create
        [HttpPost]
        public IActionResult Create([Bind("CategoryName,Weight,OriginWarehouse,DestinationWarehouse")] CitiesDTO citiesDTO)
        {
            if(!this.ModelState.IsValid)
            {
                return this.CityView(citiesDTO);
            }

            this.createCities.CreateCity(citiesDTO);

            return this.RedirectToAction(nameof(this.Index));
        }

        private IActionResult ParcelNotFound(int id)
        {
            this.Response.StatusCode = StatusCodes.Status404NotFound;
            this.ViewData["error"] = $"Parcel with id {id} does not exist.";
            return this.View("Error");
        }

        private IActionResult CityView(CitiesDTO citiesDTO)
        {
            return this.View(citiesDTO);
        }
    }
}
