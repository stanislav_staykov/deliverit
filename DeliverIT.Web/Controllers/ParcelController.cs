﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DeliverIT.Web.Controllers
{
    public class ParcelController : Controller
    {
        private ICRUDandFilterParcels cRUDandFilterParsels;
        private ModelMapper modelMapper;

        public ParcelController(ICRUDandFilterParcels cRUDandFilterParsels, ModelMapper modelMapper)
        {
            this.cRUDandFilterParsels = cRUDandFilterParsels;
            this.modelMapper = modelMapper;
        }

        public IActionResult Index()
        {
            //string email = "Ivanivi@gmail.com";
            var parcels = this.cRUDandFilterParsels.ListingParcels();

            return this.View(parcels);
        }

        //GET: /parcel/details/:id
        public IActionResult Details(int id)
        {
            try
            {
                var parcel = this.cRUDandFilterParsels.GetParcelById(id);

                return this.View(parcel);
            }
            catch(ArgumentException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                this.ViewData["error"] = $"Parcel withid= {id} does not exist.";

                return this.View("Error");
            }
        }

        //GET: /parcel/create
        public IActionResult Create()
        {
            if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            var newParcelViewModel = new ParcelViewModel();

            return this.ParcelView(newParcelViewModel);
        }

        //POST: /parcel/create
        [HttpPost]
        public IActionResult Create([Bind("CategoryName,Weight,OriginWarehouse,DestinationWarehouse")] ParcelViewModel parcelViewModel)
        {
            string email = "Ivanivi@gmail.com";
            if(!this.ModelState.IsValid)
            {
                return this.ParcelView(parcelViewModel);
            }
            try
            {
                this.cRUDandFilterParsels.Create(parcelViewModel, email);

                return this.RedirectToAction(nameof(this.Index));
            }
            catch(ArgumentException)
            {
                return this.WarehouseNotFound($"No Warehouse whit name {parcelViewModel.DestinationWarehouse}!");
            }
        }


        //GET: /parcel/edit/:id
        public IActionResult Edit(int id)
        {
            try
            {
                if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
                {
                    return this.RedirectToAction("Login", "Auth");
                }
                var parcel = this.cRUDandFilterParsels.GetParcelById(id);
                var parcelViewModel = this.modelMapper.ToParcelViewModel(parcel);

                return this.ParcelView(parcelViewModel);

            }
            catch(ArgumentException)
            {
                return this.ParcelNotFound(id);
            }
        }

        //POST: /parcel/edit/:id
        [HttpPost]
        public IActionResult Edit(int id, [Bind("Id,Weight")] ParcelViewModel parcelViewModel)
        {
            string email = "Ivanivi@gmail.com";
            if(!ModelState.IsValid)
            {
                return this.ParcelView(parcelViewModel);
            }

            try
            {
                this.cRUDandFilterParsels.Modify(email, id, double.Parse(parcelViewModel.Weight));
            }
            catch(ArgumentException)
            {
                return this.ParcelNotFound(id);
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        //GET: /parcel/delete/:id
        public IActionResult Delete(int id)
        {
            try
            {
                if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
                {
                    return this.RedirectToAction("Login", "Auth");
                }

                var parcel = this.cRUDandFilterParsels.GetParcelById(id);

                return this.View(parcel);
            }
            catch(ArgumentException)
            {
                return this.ParcelNotFound(id);
            }
        }

        //POST: /parcel/delete/:id
        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                string email = "Ivanivi@gmail.com";
                this.cRUDandFilterParsels.Delete(email, id);
            }
            catch(ArgumentException)
            {
                return this.ParcelNotFound(id);
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        private IActionResult ParcelNotFound(int id)
        {
            this.Response.StatusCode = StatusCodes.Status404NotFound;
            this.ViewData["error"] = $"Parcel with id {id} does not exist.";
            return this.View("Error");
        }

        private IActionResult WarehouseNotFound(string ex)
        {
            this.Response.StatusCode = StatusCodes.Status404NotFound;
            this.ViewData["error"] = ex;
            return this.View("Error");
        }

        private IActionResult ParcelView(ParcelViewModel parcelViewModel)
        {
            parcelViewModel.Categories = this.GetCategories();
            return this.View(parcelViewModel);
        }

        private SelectList GetCategories()
        {
            var categories = this.cRUDandFilterParsels.GetAllCategories();
            return new SelectList(categories, "Id", "Name");
        }
    }
}
