﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DeliverIT.Web.Controllers
{
    public class CountryController : Controller
    {
        private IListAllCountriesCities listAllCountriesCities;
        private ICreateCountries createCountries;
        private ModelMapper modelMapper;

        public CountryController(IListAllCountriesCities listAllCountriesCities, ICreateCountries createCountries, ModelMapper modelMapper)
        {
            this.listAllCountriesCities = listAllCountriesCities;
            this.createCountries = createCountries;
            this.modelMapper = modelMapper;
        }

        public IActionResult Index()
        {
            var parcels = this.listAllCountriesCities.ListingCountriesFotWeb();

            return this.View(parcels);
        }

        //GET: /country/details/:id
        public IActionResult Details(int id)
        {
            try
            {
                var country = this.listAllCountriesCities.GetCountryById(id);
                var countryViewModel = this.modelMapper.ToCountryModel(country, id);
                return this.View(countryViewModel);
            }
            catch(ArgumentException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                this.ViewData["error"] = $"Parcel withid= {id} does not exist.";

                return this.View("Error");
            }
        }

        //GET: /country/create
        public IActionResult Create()
        {
            if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            var country = new CountriesDTO();

            return this.CountryView(country);
        }

        //POST: /country/create
        [HttpPost]
        public IActionResult Create([Bind("CategoryName,Weight,OriginWarehouse,DestinationWarehouse")] CountriesDTO countriesDTO)
        {
            if(!this.ModelState.IsValid)
            {
                return this.CountryView(countriesDTO);
            }

            this.createCountries.CreateCountry(countriesDTO);

            return this.RedirectToAction(nameof(this.Index));
        }

        private IActionResult ParcelNotFound(int id)
        {
            this.Response.StatusCode = StatusCodes.Status404NotFound;
            this.ViewData["error"] = $"Parcel with id {id} does not exist.";
            return this.View("Error");
        }

        private IActionResult CountryView(CountriesDTO countriesDTO)
        {
            return this.View(countriesDTO);
        }
    }
}
