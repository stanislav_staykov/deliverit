﻿using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        //GET: /home/about
        public IActionResult About()
        {
            return View();
        }

        //GET: /home/noauthorisation
        public IActionResult NoAuthorisation()
        {
            return View();
        }
    }
}
