﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DeliverIT.Web.Controllers
{
    public class ShipmentController : Controller
    {
        private ICRUDandFilterShipments cRUDandFilterShipments;
        private ModelMapper modelMapper;

        public ShipmentController(ICRUDandFilterShipments cRUDandFilterShipments, ModelMapper modelMapper)
        {
            this.cRUDandFilterShipments = cRUDandFilterShipments;
            this.modelMapper = modelMapper;
        }

        public IActionResult Index()
        {
            //string email = "Ivanivi@gmail.com";
            var shipment = this.cRUDandFilterShipments.Listing();

            return this.View(shipment);
        }

        //GET: /shipment/details/:id
        public IActionResult Details(int id)
        {
            try
            {
                var shipment = this.cRUDandFilterShipments.GetShipmentById(id);

                return this.View(shipment);
            }
            catch(ArgumentException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                this.ViewData["error"] = $"Parcel withid= {id} does not exist.";

                return this.View("Error");
            }
        }

        //GET: /shipment/create
        public IActionResult Create()
        {
            if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction("Login", "Auth");
            }

            var newShipmentViewModel = new ShipmentViewModel();

            return this.ShipmentView(newShipmentViewModel);
        }

        //POST: /shipment/create
        [HttpPost]
        public IActionResult Create([Bind("CategoryName,Weight,OriginWarehouse,DestinationWarehouse")] ShipmentViewModel shipmentViewModel)
        {
            string email = "Ivanivi@gmail.com";
            if(!this.ModelState.IsValid)
            {
                return this.ShipmentView(shipmentViewModel);
            }

            this.cRUDandFilterShipments.Create(shipmentViewModel.OriginWarehouse, shipmentViewModel.DestinationWarehouse, email);

            return this.RedirectToAction(nameof(this.Index));
        }


        //GET: /shipment/edit/:id
        public IActionResult Edit(int id)
        {
            try
            {
                if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
                {
                    return this.RedirectToAction("Login", "Auth");
                }

                var shipment = this.cRUDandFilterShipments.GetShipmentById(id);
                var shipmentViewModel = this.modelMapper.ToShipmentViewModel(shipment);

                return this.ShipmentView(shipmentViewModel);
            }
            catch(ArgumentException)
            {
                return this.ShipmentNotFound(id);
            }
        }

        //POST: /shipment/edit/:id
        [HttpPost]
        public IActionResult Edit(int id, [Bind("DestinationWarehouse")] ShipmentViewModel shipmentViewModel)
        {
            string email = "Ivanivi@gmail.com";
            if(!ModelState.IsValid)
            {
                return this.ShipmentView(shipmentViewModel);
            }

            try
            {
                this.cRUDandFilterShipments.Modify(email, id, shipmentViewModel.DestinationWarehouse);
            }
            catch(ArgumentException)
            {
                return this.ShipmentNotFound(id);
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        //GET: /shipment/delete/:id
        public IActionResult Delete(int id)
        {
            try
            {
                if(!this.HttpContext.Session.Keys.Contains("CurrentUser"))
                {
                    return this.RedirectToAction("Login", "Auth");
                }

                var parcel = this.cRUDandFilterShipments.GetShipmentById(id);

                return this.View(parcel);
            }
            catch(ArgumentException)
            {
                return this.ShipmentNotFound(id);
            }
        }

        //POST: /shipment/delete/:id
        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                string email = "Ivanivi@gmail.com";
                this.cRUDandFilterShipments.Delete(email, id);
            }
            catch(ArgumentException)
            {
                return this.ShipmentNotFound(id);
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        private IActionResult ShipmentNotFound(int id)
        {
            this.Response.StatusCode = StatusCodes.Status404NotFound;
            this.ViewData["error"] = $"Parcel with id {id} does not exist.";
            return this.View("Error");
        }

        private IActionResult ShipmentView(ShipmentViewModel shipmentViewModel)
        {
            shipmentViewModel.Warehouses = this.GetWarehouses();
            return this.View(shipmentViewModel);
        }

        private SelectList GetWarehouses()
        {
            var warehouses = this.cRUDandFilterShipments.GetAllWarehouses();
            return new SelectList(warehouses, "Id", "Name");
        }
    }
}
