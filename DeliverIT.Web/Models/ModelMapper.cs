﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Web.Models
{
    public class ModelMapper
    {
        public ParcelViewModel ToParcelViewModel(Parcel parcel)
        {
            return new ParcelViewModel
            {
                CategoryName = parcel.Category.Name,
                OriginWarehouse = parcel.Shipment.OriginWarehouse.Name,
                DestinationWarehouse = parcel.Shipment.DestinationWarehouse.Name,
                Weight = parcel.Weight.ToString()
            };
        }

        public ShipmentViewModel ToShipmentViewModel(Shipment shipment)
        {
            return new ShipmentViewModel
            {
                Departure = shipment.Departure.ToString(),
                Arrival = shipment.Arrival.ToString(),
                Status = shipment.Status.ToString(),
                OriginWarehouse = shipment.OriginWarehouse.Name,
                DestinationWarehouse = shipment.DestinationWarehouse.Name
            };
        }

        public CityViewModel ToCityModel(CitiesDTO cityDTO, int id)
        {
            return new CityViewModel
            {
                Id = id,
                City = cityDTO.City,
                Country = cityDTO.Country
            };
        }

        public CountryViewModel ToCountryModel(CountriesDTO countriesDTO, int id)
        {
            return new CountryViewModel
            {
                Id = id,
                Country = countriesDTO.Country
            };
        }

        public WarehouseViewModel ToWarehouseViewModel(Warehouse warehouse)
        {
            return new WarehouseViewModel
            {
                Id = warehouse.Id,
                Warehouse = warehouse.Name,
                StreetName = warehouse.Address.StreetName,
                City = warehouse.Address.City.Name,
                Country = warehouse.Address.City.Country.Name
            };
        }
    }
}
