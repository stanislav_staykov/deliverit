﻿using System.ComponentModel.DataAnnotations;

namespace AspNetCoreDemo.Web.Models
{
    public class LoginViewModel
    {
        [Required]
        public string Email { get; set; }
    }
}
