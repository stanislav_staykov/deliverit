﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Web.Contracts;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DeliverIT.Web.Models
{
    public class WarehouseViewModel : WarehouseDTO, ICityViewModel
    {
        public int Id { get; set; }
        public string NewWarehouseName { get; set; }
        public SelectList Addresses { get; set; }
    }
}
