﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Web.Contracts;

namespace DeliverIT.Web.Models
{
    public class CountryViewModel: ICountryViewModel
    {
        public int Id { get; set; }
        public string Country { get; set; }
    }
}
