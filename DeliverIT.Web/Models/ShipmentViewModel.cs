﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DeliverIT.Web.Models
{
    public class ShipmentViewModel : ShipmentDTO
    {
        public SelectList Warehouses { get; set; }
    }
}
