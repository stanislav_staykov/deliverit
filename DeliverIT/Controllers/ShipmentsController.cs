﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data.Models;
using DeliverIT.Services;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ShipmentsController : ControllerBase
    {
        public readonly IUserService userServise;
        private readonly ICRUDandFilterParcels listCreateModifyParcels;
        private readonly ICRUDandFilterShipments cRUDandFilterShipments;

        public ShipmentsController(IUserService userServise,
                                   ICRUDandFilterParcels listCreateModifyParcels,
                                   ICRUDandFilterShipments cRUDandFilterShipments)
        {
            this.userServise = userServise;
            this.listCreateModifyParcels = listCreateModifyParcels;
            this.cRUDandFilterShipments = cRUDandFilterShipments;
        }

        [HttpPost("Add")]
        public IActionResult Post([FromBody] ParcelDTO shipment)
        {
            // not working
            if(shipment == null)
            {
                return this.BadRequest("Insert data!");
            }
            if(this.Request.Cookies.ContainsKey("Login"))
            {
                var email = this.Request.Cookies["Login"];
                var userAtoritythis = this.userServise.Get(email);
                if(userAtoritythis == null)
                {
                    return BadRequest("Unauthorised access!");
                }
                if(userAtoritythis == Services.Enum.Access.Customer && shipment != null)
                {
                    try
                    {
                        var result = this.listCreateModifyParcels.Create(shipment, email);
                        return this.Ok(result);
                    }
                    catch(ArgumentException ex)
                    {
                        return this.BadRequest(ex.Message);
                    }
                }
            }
            return this.Redirect("localhost:5000/api/login");

        }
        [HttpGet("List")]
        public IActionResult List()
        {
            if(this.Request.Cookies.ContainsKey("Login"))
            {
                var email = this.Request.Cookies["Login"];
                var userAtoritythis = this.userServise.Get(email);
                if(userAtoritythis == null)
                {
                    return BadRequest("Unauthorised access!");
                }
                if(userAtoritythis == Services.Enum.Access.Customer)
                {
                    List<ShipmentDTO> result = this.cRUDandFilterShipments.Listing(email);
                    return Ok(result);
                }
                return this.BadRequest("No authorization for this action.");
            }
            return this.Redirect("localhost:5000/api/login");
        }

        [HttpPost("Modify")]
        public IActionResult Modify([FromBody] Shipment shipment)
        {
            if(this.Request.Cookies.ContainsKey("Login"))
            {
                var email = this.Request.Cookies["Login"];
                var userAtoritythis = this.userServise.Get(email);
                if(userAtoritythis == null)
                {
                    return BadRequest("Unauthorised access!");
                }
                if(userAtoritythis == Services.Enum.Access.Customer)
                {
                    try
                    {
                        ShipmentDTO result = this.cRUDandFilterShipments.Modify(email, shipment.Id, shipment.DestinationWarehouseId);
                        return Ok(result);
                    }
                    catch(ArgumentException ex)
                    {
                        return this.BadRequest(ex.Message);
                    }
                }
                return this.BadRequest("No authorization for this action.");
            }
            return this.Redirect("localhost:5000/api/login");
        }

        [HttpDelete("Delete")]
        public IActionResult Delete([FromBody] Shipment shipment)
        {
            if(this.Request.Cookies.ContainsKey("Login"))
            {
                var email = this.Request.Cookies["Login"];
                var userAtoritythis = this.userServise.Get(email);
                if(userAtoritythis == null)
                {
                    return BadRequest("Unauthorised access!");
                }
                if(userAtoritythis == Services.Enum.Access.Customer)
                {
                    try
                    {
                        ShipmentDTO result = this.cRUDandFilterShipments.Delete(email, shipment.Id);
                        return Ok(result);
                    }
                    catch(ArgumentException ex)
                    {
                        return this.BadRequest(ex.Message);
                    }
                }
                return this.BadRequest("No authorization for this action.");
            }
            return this.Redirect("localhost:5000/api/login");
        }

        [HttpGet("Filter")]
        public IActionResult Filter([FromQuery] string warehouse)
        {
            List<ShipmentDTO> result = this.cRUDandFilterShipments.Filter(warehouse);
            return Ok(result);
        }

        [HttpGet("FilterByCustomer")]
        public IActionResult FilterByCustomer([FromQuery] string customerName)
        {
            List<ShipmentDTO> result = this.cRUDandFilterShipments.FilterPerson(customerName);
            return Ok(result);
        }
    }
}
