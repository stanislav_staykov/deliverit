﻿using System;
using System.Collections.Generic;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICRUDandSearchCustomer cRUDandSearchCustomer;

        public CustomerController(ICRUDandSearchCustomer cRUDandSearchCustomer)
        {
            this.cRUDandSearchCustomer = cRUDandSearchCustomer;
        }

        [HttpGet("")]
        public IActionResult Get()
        {
            List<CustomerDTO> result = this.cRUDandSearchCustomer.Listing();
            
            return Ok(result);
        }

        [HttpGet("{name}")]
        public IActionResult Get(string name )
        {
            if(name == null)
            {
                return BadRequest();
            }

            try
            {
                CustomerDTO result = this.cRUDandSearchCustomer.Get(name);
                return Ok(result);
            }
            catch(ArgumentNullException ex)
            {
                return this.NotFound(ex.Message);
            }            
        }

        [HttpGet("AvailableWarehouses")]
        public IActionResult GetWarehouses()
        {
            List<WarehouseDTO> result = this.cRUDandSearchCustomer.GetAllWarehouses();

            return Ok(result);
        }

        [HttpPost("Modify")]
        public IActionResult ModifyCustomer([FromQuery] string email, string newEmail)
        {
            if(email == null || newEmail ==null)
            {
                return BadRequest();
            }

            CustomerDTO result = this.cRUDandSearchCustomer.Modify(email, newEmail);

            return Ok(result);
        }

        [HttpDelete("Delete")]
        public IActionResult DeleteCustomer([FromQuery] string email)
        {
            if(email == null)
            {
                return BadRequest();
            }

            CustomerDTO result = this.cRUDandSearchCustomer.Delete(email);

            return Ok(result);
        }

        [HttpGet("SerchByEmail")]
        public IActionResult SerchByEmail([FromQuery] string email)
        {
            if(email == null)
            {
                return BadRequest();
            }

            CustomerDTO result = this.cRUDandSearchCustomer.SerchByEmail(email);

            return Ok(result);
        }

        [HttpGet("SearchByFirstName")]
        public IActionResult SearchByFirstName([FromQuery] string fName)
        {
            if(fName == null)
            {
                return BadRequest();
            }

            List<CustomerDTO> result = this.cRUDandSearchCustomer.SearchByFirstName(fName);

            return Ok(result);
        }

        [HttpGet("SearchByLastName")]
        public IActionResult SearchByLastName([FromQuery] string lName)
        {
            if(lName == null)
            {
                return BadRequest();
            }

            List<CustomerDTO> result = this.cRUDandSearchCustomer.SearchByLastName(lName);

            return Ok(result);
        }

        [HttpGet("SearchByParcelType")]
        public IActionResult SearchByParcelType([FromQuery] string email, string parcelType)
        {
            if(email == null || parcelType == null)
            {
                return BadRequest();
            }

            List<ParcelDTO> result = this.cRUDandSearchCustomer.SearchByParcelType(email, parcelType);

            return Ok(result);
        }
    }
}
