﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data.Models;
using DeliverIT.Services;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParcelController : ControllerBase
    {
        public readonly IUserService userServise;
        private readonly ICRUDandFilterParcels cRUDandFilterParsels;

        public ParcelController(IUserService userServise,
                                   ICRUDandFilterParcels cRUDandFilterParsels)
        {
            this.userServise = userServise;
            this.cRUDandFilterParsels = cRUDandFilterParsels;
        }

        [HttpPost("Add")]
        public IActionResult Post([FromBody] ParcelDTO parcel)
        {
            // not working
            if(parcel == null)
            {
                return this.BadRequest("Insert data!");
            }
            if(this.Request.Cookies.ContainsKey("Login"))
            {
                var email = this.Request.Cookies["Login"];
                var userAtoritythis = this.userServise.Get(email);
                if(userAtoritythis == null)
                {
                    return BadRequest("Unauthorised access!");
                }
                if(userAtoritythis == Services.Enum.Access.Customer && parcel != null)
                {
                    try
                    {
                        var result = this.cRUDandFilterParsels.Create(parcel, email);
                        return this.Ok(result);
                    }
                    catch(ArgumentException ex)
                    {
                        return this.BadRequest(ex.Message);
                    }
                }
            }
            return this.Redirect("localhost:5000/api/login");

        }
        [HttpGet("List")]
        public IActionResult List()
        {
            if(this.Request.Cookies.ContainsKey("Login"))
            {
                var email = this.Request.Cookies["Login"];
                var userAtoritythis = this.userServise.Get(email);
                if(userAtoritythis == null)
                {
                    return BadRequest("Unauthorised access!");
                }
                if(userAtoritythis == Services.Enum.Access.Customer)
                {
                    List<ParcelDTO> result = this.cRUDandFilterParsels.Listing(email);
                    return Ok(result);
                }
                return this.BadRequest("No authorization for this action.");
            }
            return this.Redirect("localhost:5000/api/login");
        }

        [HttpPost("Modify")]
        public IActionResult Modify([FromBody] Parcel parcelData)
        {
            if(this.Request.Cookies.ContainsKey("Login"))
            {
                var email = this.Request.Cookies["Login"];
                var userAtoritythis = this.userServise.Get(email);
                if(userAtoritythis == null)
                {
                    return BadRequest("Unauthorised access!");
                }
                if(userAtoritythis == Services.Enum.Access.Customer)
                {
                    try
                    {
                        ParcelDTO result = this.cRUDandFilterParsels.Modify(email, parcelData.Id, parcelData.Weight);
                        return Ok(result);
                    }
                    catch(ArgumentException ex)
                    {
                        return this.BadRequest(ex.Message);
                    }
                }
                return this.BadRequest("No authorization for this action.");
            }
            return this.Redirect("localhost:5000/api/login");
        }

        [HttpDelete("Delete")]
        public IActionResult Delete([FromBody] Parcel parcelData)
        {
            if(this.Request.Cookies.ContainsKey("Login"))
            {
                var email = this.Request.Cookies["Login"];
                var userAtoritythis = this.userServise.Get(email);
                if(userAtoritythis == null)
                {
                    return BadRequest("Unauthorised access!");
                }
                if(userAtoritythis == Services.Enum.Access.Customer)
                {
                    try
                    {
                        ParcelDTO result = this.cRUDandFilterParsels.Delete(email, parcelData.Id);
                        return Ok(result);
                    }
                    catch(ArgumentException ex)
                    {
                        return this.BadRequest(ex.Message);
                    }
                }
                return this.BadRequest("No authorization for this action.");
            }
            return this.Redirect("localhost:5000/api/login");
        }

        [HttpGet("FilterByWeight")]
        public IActionResult FilterByWeight([FromQuery] double weight)
        {
            try
            {
            List<ParcelDTO> result = this.cRUDandFilterParsels.FilterByWeight(weight);

            return Ok(result);
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("FilterByWarehouse")]
        public IActionResult FilterByWarehouse([FromQuery] string warehouseName)
        {
            try
            {
            List<ParcelDTO> result = this.cRUDandFilterParsels.FilterByWarehouse(warehouseName);

            return Ok(result);
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("FilterByCustomer")]
        public IActionResult FilterByCustomer([FromQuery] string customerName)
        {
            try
            {
            List<ParcelDTO> result = this.cRUDandFilterParsels.FilterByCustomer(customerName);
            
            return Ok(result);
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("FilterByCategory")]
        public IActionResult FilterByCategory([FromQuery] string categoryName)
        {
            try
            {
            List<ParcelDTO> result = this.cRUDandFilterParsels.FilterByCategory(categoryName);

            return Ok(result);
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}