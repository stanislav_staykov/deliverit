﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginRegisterController : ControllerBase
    {
        public readonly IUserService userServise;
        private readonly ICRUDandSearchCustomer cRUDandSearchCustomer;

        public LoginRegisterController(IUserService userServise, ICRUDandSearchCustomer cRUDandSearchCustomer)
        {
            this.userServise = userServise;
            this.cRUDandSearchCustomer = cRUDandSearchCustomer;
        }
        [HttpGet("Login")]
        public IActionResult Login([FromHeader] string credentials)
        {
            if(credentials == null)
            {
                return this.BadRequest("You need to fill your credentials!");
            }
            if(this.userServise.GetEmail(credentials))
            {
                CookieOptions options = new CookieOptions();
                options.Expires = DateTime.Now.AddMinutes(3);
                this.Response.Cookies.Append("Login", credentials);
                return this.Ok("You login successful.");
            }
            return this.BadRequest("This credentials not exist!");
        }

        [HttpPost("Register")]
        public IActionResult Register([FromBody] RegisterCustomerDTO customerdata)
        {
            if(customerdata == null)
            {
                return this.BadRequest("You need to send necessary data!");
            }
            if(this.userServise.GetEmail(customerdata.Email))
            {
                return this.BadRequest("Usere with that name already exist!");
            }
            try
            {
                var result = this.cRUDandSearchCustomer.Create(customerdata);
            }
            catch(ArgumentException ex)
            {
                return this.BadRequest(ex.Message);
            }

            CookieOptions options = new CookieOptions();
            options.Expires = DateTime.Now.AddMinutes(3);
            this.Response.Cookies.Append("Login", customerdata.Email);
            return this.Ok("You were registered and logged successfully.");
        }
    }
}
