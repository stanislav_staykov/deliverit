﻿using System.Collections.Generic;
using DeliverIT.Services;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CountriesController : ControllerBase
    {
        private IListAllCountriesCities listAllCountriesCities;

        public CountriesController(IListAllCountriesCities listAllCountriesCities)
        {
            this.listAllCountriesCities = listAllCountriesCities;
        }
        [HttpGet("")]
        public IActionResult Get()
        {
            List<CountriesDTO> result = this.listAllCountriesCities.ListingCountries();

            return Ok(result);
        }
    }
}
