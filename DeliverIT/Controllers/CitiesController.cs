﻿using System.Collections.Generic;
using DeliverIT.Services;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CitiesController : ControllerBase
    {
        private IListAllCountriesCities listAllCountriesCities;

        public CitiesController(IListAllCountriesCities listAllCountriesCities)
        {
            this.listAllCountriesCities = listAllCountriesCities;
        }
        [HttpGet("")]
        public IActionResult Get()
        {
            List<CitiesDTO> result = this.listAllCountriesCities.ListingCities();

            return Ok(result);
        }
    }
}
