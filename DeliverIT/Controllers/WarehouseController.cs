﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WarehouseController : ControllerBase
    {
        private IListCreateModifyDeleteWarehouses listCreateModifyDeleteWarehouses;

        public WarehouseController(IListCreateModifyDeleteWarehouses listCreateModifyDeleteWarehouses)
        {
            this.listCreateModifyDeleteWarehouses = listCreateModifyDeleteWarehouses;
        }
        [HttpGet("List")]
        public IActionResult Get()
        {
            List<WarehouseDTO> result = this.listCreateModifyDeleteWarehouses.Listing();

            return Ok(result);
        }

        [HttpPost("Create")]
        public IActionResult Create([FromBody] WarehouseDTO warehouse)
        {
            try
            {
                WarehouseDTO result = this.listCreateModifyDeleteWarehouses.Create(warehouse);

                return Ok(result);
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }         
        }

        [HttpPut("Modify")]
        public IActionResult Modify([FromQuery] string warehouse, string newName)
        {
            WarehouseDTO result = this.listCreateModifyDeleteWarehouses.Modify(warehouse, newName);

            return Ok(result);
        }
    }
}
