using DeliverIT.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using DeliverIT.Services;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;

namespace DeliverIT
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DeliverItContext>(options =>
            {
                options.UseSqlServer(@"Server=DESKTOP-3VF5E9T\SQLEXPRESS;Database=DeliverIt;Integrated Security=True");
            });
            services.AddControllers().AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICheckingValues, CheckingValues>();
            services.AddScoped<ICreateAddresses, CreateAddresses>();
            services.AddScoped<ICreateCities, CreateCities>();
            services.AddScoped<ICreateCountries, CreateCountries>();
            services.AddScoped<RegisterCustomerDTO>();
            services.AddScoped<ICRUDandFilterParcels, CRUDandFilterParcels>();
            services.AddScoped<ICRUDandFilterShipments, CRUDandFilterShipments>();
            services.AddScoped<IListAllCountriesCities, ListAllCountriesCities>();
            services.AddScoped<IListCreateModifyDeleteWarehouses, ListCreateModifyDeleteWarehouses>();
            services.AddScoped<ICRUDandSearchCustomer, CRUDandSearchCustomer>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if(env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
