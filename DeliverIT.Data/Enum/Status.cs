﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Data.Enum
{
    public enum Status
    {
        preparing = 0,
        on_the_way = 1,
        completed = 2
    }
}
