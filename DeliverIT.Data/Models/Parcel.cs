﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Data.Models
{
    public class Parcel
    {
        [Key]
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public int WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int ShipmentId { get; set; }
        public Shipment Shipment { get; set; }       
        public double Weight { get; set; }

    }
}
