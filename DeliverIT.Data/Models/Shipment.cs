﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using DeliverIT.Data.Enum;

namespace DeliverIT.Data.Models
{
    public class Shipment
    {
        [Key]
        public int Id { get; set; }
        public DateTime Departure { get; set; }
        public DateTime Arrival { get; set; }
        public Status Status { get; set; }
        public int OriginWarehouseId { get; set; }

        [InverseProperty("OriginShipment")]
        public Warehouse OriginWarehouse { get; set; }
        public int DestinationWarehouseId { get; set; }

        [InverseProperty("DestinationShipment")]
        public Warehouse DestinationWarehouse { get; set; }
        public ICollection<Parcel> Parcels { get; set; } = new HashSet<Parcel>();
    }
}
