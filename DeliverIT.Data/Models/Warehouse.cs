﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Data.Models
{
    public class Warehouse
    {
        [Key]
        public int Id { get; set; }
        [Required, RegularExpression("[A-Z]"), StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} should be between {1} and {2} characters.")]
        public string Name { get; set; }
        public int AddressId { get; set; }
        public Address Address { get; set; }
        public ICollection<Shipment> OriginShipment { get; set; }
        public ICollection<Shipment> DestinationShipment { get; set; }
        public ICollection<Parcel> Parcels { get; set; }
    }
}