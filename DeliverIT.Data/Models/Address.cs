﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DeliverIT.Data.Models
{
    public class Address
    {
        [Key]
        public int Id { get; set; }

        [Required, RegularExpression("[A-Z]", ErrorMessage = "Value for {0} should start with capital letter!"), ]
        public string StreetName { get; set; }
        public int CityID { get; set; }
        public City City { get; set; }

        //public Warehouse Warehouse { get; set; }
        //public ICollection<Employee> Employees { get; set; } = new HashSet<Employee>();
        //public ICollection<Customer> Customers { get; set; } = new HashSet<Customer>();
    }
}
