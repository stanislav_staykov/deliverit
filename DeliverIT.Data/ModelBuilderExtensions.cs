﻿using System;
using System.Collections.Generic;
using DeliverIT.Data.Enum;
using DeliverIT.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Data
{
    public static class ModelBuilderExtensions
    {
        public static IEnumerable<Country> Countries { get; }
        public static IEnumerable<City> Cities { get; }
        public static IEnumerable<Address> Addresses { get; }
        public static IEnumerable<Employee> Employees { get; }
        public static IEnumerable<Category> Categories { get; }
        public static IEnumerable<Customer> Customers { get; }
        public static IEnumerable<Warehouse> Warehouses { get; }
        public static IEnumerable<Parcel> Parcels { get; }
        public static IEnumerable<Shipment> Shipments { get; }

        static ModelBuilderExtensions()
        {
            Countries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name ="Bulgaria"
                },
                new Country
                {
                    Id = 2,
                    Name = "Turkey"
                }
            };
            Cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Plovdiv",
                    CountryId = 1
                },
                new City
                {
                    Id = 2,
                    Name = "Sofia",
                    CountryId = 1
                }
            };
            Addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    StreetName = "Lerin 21",
                    CityID = 1
                },
                new Address
                {
                    Id = 2,
                    StreetName = "Voinishka Slava 10",
                    CityID = 1
                },
                new Address
                {
                    Id = 3,
                    StreetName = "Sevastopol 5",
                    CityID = 1
                },
                new Address
                {
                    Id = 4,
                    StreetName = "Sevastopol 30",
                    CityID = 1
                },
                new Address
                {
                    Id = 5,
                    StreetName = "Levski 4",
                    CityID = 1
                },
                new Address
                {
                    Id = 6,
                    StreetName = "Borba 3",
                    CityID = 1
                },
                new Address
                {
                    Id = 7,
                    StreetName = "Slava 4",
                    CityID = 1
                },
                new Address
                {
                    Id = 8,
                    StreetName = "Kitka 8",
                    CityID = 1
                }
            };
            Customers = new List<Customer>
            {
                 new Customer
                 {
                     Id = 1,
                    FirstName = "Ivan",
                    LastName = "Ivanov",
                    Email = "Ivanivi@gmail.com",
                    AddressId = 1
                 },
                 new Customer
                 {
                     Id = 2,
                     FirstName = "Maria",
                     LastName = "Petrova",
                     Email = "Mims44@abv.bg",
                     AddressId = 2
                 },
                 new Customer
                 {
                     Id = 3,
                     FirstName = "Petur",
                     LastName = "Petrov",
                     Email = "Pepi123@gmail.com",
                     AddressId = 3
                 }
            };
            Employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Georgi",
                    LastName = "Kirov",
                    Email = "Kirov99@abv.bg",
                    AddressId = 4
                },
                new Employee
                {
                    Id = 2,
                    FirstName = "Jivko",
                    LastName = "Jivov",
                    Email = "Jivko3@abv.bg",
                    AddressId = 5
                }
            };
            Warehouses = new List<Warehouse>
            {
                new Warehouse
                {
                    Id = 1,
                    Name = "Ikea",
                    AddressId = 6
                },
                new Warehouse
                {
                    Id = 2,
                    Name = "Jysk",
                    AddressId = 7
                }
            };
            Categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Garden"
                },
                new Category
                {
                    Id = 2,
                    Name = "Furniture"
                },
            };
            Parcels = new List<Parcel>
            {
                new Parcel
                {
                    Id = 1,
                    CustomerId = 1,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 1,
                    Weight = 10
                },
                new Parcel
                {
                    Id = 2,
                    CustomerId = 2,
                    WarehouseId = 2,
                    CategoryId = 1,
                    ShipmentId = 2,
                    Weight = 1
                }
            };
            Shipments = new List<Shipment>
            {
                new Shipment
                {
                    Id = 1,
                    Departure = new DateTime(2021, 1, 1),
                    Arrival = new DateTime(2021, 2, 2),
                    Status = Status.preparing,
                    OriginWarehouseId = 1,
                    DestinationWarehouseId = 2
                },
                new Shipment
                {
                    Id = 2,
                    Departure = new DateTime(2021, 3, 3),
                    Arrival = new DateTime(2021, 5, 5),
                    Status = Status.preparing,
                    OriginWarehouseId = 2,
                    DestinationWarehouseId = 1
                }
            };
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().HasData(Countries);
            modelBuilder.Entity<City>().HasData(Cities);
            modelBuilder.Entity<Address>().HasData(Addresses);
            modelBuilder.Entity<Employee>().HasData(Employees);
            modelBuilder.Entity<Category>().HasData(Categories);
            modelBuilder.Entity<Customer>().HasData(Customers);
            modelBuilder.Entity<Warehouse>().HasData(Warehouses);
            modelBuilder.Entity<Parcel>().HasData(Parcels);
            modelBuilder.Entity<Shipment>().HasData(Shipments);

            modelBuilder.Entity<Shipment>()
                .HasOne(s => s.OriginWarehouse)
                .WithMany(s => s.OriginShipment)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Shipment>()
                .HasOne(s => s.DestinationWarehouse)
                .WithMany(s => s.DestinationShipment)
                .OnDelete(DeleteBehavior.ClientSetNull);

            modelBuilder.Entity<Parcel>()
                .HasOne(p => p.Warehouse)
                .WithMany(p => p.Parcels)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
