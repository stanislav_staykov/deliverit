﻿using DeliverIT.Data;
using DeliverIT.Data.Enum;
using DeliverIT.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace AspNetCoreDemo.Tests.Services
{
    public class Utils
    {
        public static DbContextOptions<DeliverItContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<DeliverItContext>()
                       .UseInMemoryDatabase(databaseName)
                       .Options;
        }

        //public static IEnumerable<Address> GetAddresses()
        //{
        //    return ModelBuilderExtensions.Addresses;
        //}

        //public static IEnumerable<Country> GetCountries()
        //{
        //    return ModelBuilderExtensions.Countries;
        //}

        //public static IEnumerable<City> GetCities()
        //{
        //    return ModelBuilderExtensions.Cities;
        //}

        //public static IEnumerable<Category> GetCategories()
        //{
        //    return ModelBuilderExtensions.Categories;
        //}

        //public static IEnumerable<Customer> GetCustomers()
        //{
        //    return ModelBuilderExtensions.Customers;
        //}

        //public static IEnumerable<Employee> GetEmployees()
        //{
        //    return ModelBuilderExtensions.Employees;
        //}

        //public static IEnumerable<Parcel> GetParcels()
        //{
        //    return ModelBuilderExtensions.Parcels;
        //}

        //public static IEnumerable<Shipment> GetShipments()
        //{
        //    return ModelBuilderExtensions.Shipments;
        //}

        //public static IEnumerable<Warehouse> GetWarehouses()
        //{
        //    return ModelBuilderExtensions.Warehouses;
        //}
        public static void Seed(DbContext dbContext)
        {
            var countries = new List<Country>
            {
                new Country
                {
                    Id = 1,
                    Name ="Bulgaria"
                },
                new Country
                {
                    Id = 2,
                    Name = "Turkey"
                }
            };
            dbContext.AddRange(countries);

            var cities = new List<City>
            {
                new City
                {
                    Id = 1,
                    Name = "Plovdiv",
                    CountryId = 1
                },
                new City
                {
                    Id = 2,
                    Name = "Sofia",
                    CountryId = 1
                }
            };
            dbContext.AddRange(cities);

            var addresses = new List<Address>
            {
                new Address
                {
                    Id = 1,
                    StreetName = "Lerin 21",
                    CityID = 1
                },
                new Address
                {
                    Id = 2,
                    StreetName = "Voinishka Slava 10",
                    CityID = 1
                },
                new Address
                {
                    Id = 3,
                    StreetName = "Sevastopol 5",
                    CityID = 1
                },
                new Address
                {
                    Id = 4,
                    StreetName = "Sevastopol 30",
                    CityID = 1
                },
                new Address
                {
                    Id = 5,
                    StreetName = "Levski 4",
                    CityID = 1
                },
                new Address
                {
                    Id = 6,
                    StreetName = "Borba 3",
                    CityID = 1
                },
                new Address
                {
                    Id = 7,
                    StreetName = "Slava 4",
                    CityID = 1
                },
                new Address
                {
                    Id = 8,
                    StreetName = "Kitka 8",
                    CityID = 1
                }
            };
            dbContext.AddRange(addresses);

            var customers = new List<Customer>
            {
                 new Customer
                 {
                     Id = 1,
                    FirstName = "Ivan",
                    LastName = "Ivanov",
                    Email = "Ivanivi@gmail.com",
                    AddressId = 1
                 },
                 new Customer
                 {
                     Id = 2,
                     FirstName = "Maria",
                     LastName = "Petrova",
                     Email = "Mims44@abv.bg",
                     AddressId = 2
                 },
                 new Customer
                 {
                     Id = 3,
                     FirstName = "Petur",
                     LastName = "Petrov",
                     Email = "Pepi123@gmail.com",
                     AddressId = 3
                 }
            };
            dbContext.AddRange(customers);

            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = 1,
                    FirstName = "Georgi",
                    LastName = "Kirov",
                    Email = "Kirov99@abv.bg",
                    AddressId = 4
                },
                new Employee
                {
                    Id = 2,
                    FirstName = "Jivko",
                    LastName = "Jivov",
                    Email = "Jivko3@abv.bg",
                    AddressId = 5
                }
            };
            dbContext.AddRange(employees);

            var warehouses = new List<Warehouse>
            {
                new Warehouse
                {
                    Id = 1,
                    Name = "Ikea",
                    AddressId = 6
                },
                new Warehouse
                {
                    Id = 2,
                    Name = "Jysk",
                    AddressId = 7
                }
            };
            dbContext.AddRange(warehouses);

            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Garden"
                },
                new Category
                {
                    Id = 2,
                    Name = "Furniture"
                },
            };
            dbContext.AddRange(categories);

            var parcels = new List<Parcel>
            {
                new Parcel
                {
                    Id = 1,
                    CustomerId = 1,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 1,
                    Weight = 10
                },
                new Parcel
                {
                    Id = 2,
                    CustomerId = 2,
                    WarehouseId = 1,
                    CategoryId = 1,
                    ShipmentId = 2,
                    Weight = 1
                }
            };
            dbContext.AddRange(parcels);

            var shipments = new List<Shipment>
            {
                new Shipment
                {
                    Id = 1,
                    Departure = new DateTime(2021, 1, 1),
                    Arrival = new DateTime(2021, 2, 2),
                    Status = Status.preparing,
                    OriginWarehouseId = 1,
                    DestinationWarehouseId = 2
                },
                new Shipment
                {
                    Id = 2,
                    Departure = new DateTime(2021, 3, 3),
                    Arrival = new DateTime(2021, 5, 5),
                    Status = Status.preparing,
                    OriginWarehouseId = 2,
                    DestinationWarehouseId = 1
                }
            };
            dbContext.AddRange(shipments);
        }

    }

}
