﻿using DeliverIT.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AspNetCoreDemo.Tests.Services
{
    public abstract class TestPreparation
    {
        public DbContextOptions<DeliverItContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));
            using(var arrangeContext = new DeliverItContext(options))
            {
                Utils.Seed(arrangeContext);
                arrangeContext.SaveChanges();
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            var options = Utils.GetOptions(nameof(TestContext.TestName));
            var context = new DeliverItContext(options);
            context.Database.EnsureDeleted();
        }
    }
}
