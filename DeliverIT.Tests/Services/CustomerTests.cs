﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AspNetCoreDemo.Tests.Services;
using DeliverIT.Data;
using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class CustomerTests : TestPreparation
    {


        [TestMethod]
        public void GetCustomerWhitName()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {               
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);
                var customer = sut.Get("Ivan");
                Assert.AreEqual("Ivan", customer.FirstName);
                Assert.AreEqual("Ivanov", customer.LastName);
                Assert.AreEqual("Lerin 21", customer.StreetName);
                Assert.AreEqual("Ivanivi@gmail.com", customer.Email);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Thare is no customer with name.")]
        public void ThrowArgumentExceptionIfThereIsNoCustomerWhitThatName()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);
                var customer = sut.Get("Ivancho");
            }
        }

        [TestMethod]
        public void GetAllWarehouses()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);
                var customer = sut.GetAllWarehouses();
                var allWarehouses = assertContext.Warehouses.ToList();
                Assert.AreEqual(2 , allWarehouses.Count());
                Assert.AreEqual("Ikea", allWarehouses[0].Name);
                Assert.AreEqual("Jysk", allWarehouses[1].Name);
            }
        }

        [TestMethod]
        public void GetAllCustomers()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);
                var customer = sut.Listing();
                Assert.AreEqual(3, customer.Count());
                Assert.AreEqual("Ivan", customer[0].FirstName);
                Assert.AreEqual("Maria", customer[1].FirstName);
                Assert.AreEqual("Petur", customer[2].FirstName);
            }
        }

        [TestMethod]
        public void CreateCustomer()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);
                var customerData = new RegisterCustomerDTO
                {
                    FirstName = "Milen",
                    LastName = "Milenov",
                    Email = "Melencho22@abv.bg",
                    StreetName = "Rozova Dolina 4",
                    City = "Plovdiv",
                    Country = "Bulgaria"
                };

                sut.Create(customerData);
                var newCustomer = assertContext.Customers.Where(c => c.FirstName == "Milen").FirstOrDefault();
                Assert.AreEqual("Milen", newCustomer.FirstName);
                Assert.AreEqual("Milenov", newCustomer.LastName);
                Assert.AreEqual("Melencho22@abv.bg", newCustomer.Email);
                Assert.AreEqual("Rozova Dolina 4", newCustomer.Address.StreetName);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Insert correct value for FirstName!")]
        public void IfCustomerFirsNameIsUnder3Symbols()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);
                var customerData = new RegisterCustomerDTO
                {
                    FirstName = "Mi",
                    LastName = "Midf",
                    Email = "Melencho22@abv.bg",
                    StreetName = "Rozova Dolina 4",
                    City = "Plovdiv",
                    Country = "Bulgaria"
                };
                sut.Create(customerData);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Insert correct value for LastName!")]
        public void IfCustomerLastNameIsUnder3Symbols()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);
                var customerData = new RegisterCustomerDTO
                {
                    FirstName = "Mifgd",
                    LastName = "Mi",
                    Email = "Melencho22@abv.bg",
                    StreetName = "Rozova Dolina 4",
                    City = "Plovdiv",
                    Country = "Bulgaria"
                };
                sut.Create(customerData);
            }
        }

        [TestMethod]
        public void ModifyCustomer()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                sut.Modify( "Ivanivi@gmail.com", "Melencho22@abv.bg");
                var newCustomer = assertContext.Customers.Where(c => c.Email == "Melencho22@abv.bg").FirstOrDefault();
                Assert.AreEqual("Ivan", newCustomer.FirstName);
                Assert.AreEqual("Ivanov", newCustomer.LastName);
                Assert.AreEqual("Melencho22@abv.bg", newCustomer.Email);
                Assert.AreEqual("Lerin 21", newCustomer.Address.StreetName);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No customer with that email.")]
        public void IfThereIsNoCustomerWithThatEmailThrowArgumentException()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                sut.Modify("Melencho22@abv.bg", "Melencho22@abv.bg");
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Customer with that email exist.")]
        public void IfThereIsCustomerWithThatNewEmailThrowArgumentException()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                sut.Modify("Ivanivi@gmail.com", "Ivanivi@gmail.com");
            }
        }

        [TestMethod]
        public void DeleteCustomer()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                var deletedCustomer = sut.Delete("Ivanivi@gmail.com");
                var delCustomer = assertContext.Customers.Where(c => c.Email == "Ivanivi@gmail.com").FirstOrDefault();
                Assert.IsNull(delCustomer);
                Assert.AreEqual("Ivan", deletedCustomer.FirstName);
                Assert.AreEqual("Ivanov", deletedCustomer.LastName);
                Assert.AreEqual("Ivanivi@gmail.com", deletedCustomer.Email);
                Assert.AreEqual("Lerin 21", deletedCustomer.StreetName);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No customer with that email.")]
        public void IfThereIsCustomerWithThatEmailThrowArgumentException()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                sut.Delete("Melencho22@abv.bg");
            }
        }

        [TestMethod]
        public void SerchCustomerByEmail()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                var customer = sut.SerchByEmail("Ivanivi@gmail.com");
                Assert.AreEqual("Ivan", customer.FirstName);
                Assert.AreEqual("Ivanov", customer.LastName);
                Assert.AreEqual("Ivanivi@gmail.com", customer.Email);
                Assert.AreEqual("Lerin 21", customer.StreetName);
            }
        }

        [TestMethod]
        public void SerchCustomerByFirstName()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                var customer = sut.SearchByFirstName("Ivan");
                Assert.AreEqual(1, customer.Count());
                Assert.AreEqual("Ivan", customer[0].FirstName);
                Assert.AreEqual("Ivanov", customer[0].LastName);
                Assert.AreEqual("Ivanivi@gmail.com", customer[0].Email);
                Assert.AreEqual("Lerin 21", customer[0].StreetName);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No customer with that first name.")]
        public void IfThereIsNpCustomerWithThatNameThrowArgumentException()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                sut.SearchByFirstName("Milen");
            }
        }

        [TestMethod]
        public void SerchCustomerByLastName()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                var customer = sut.SearchByLastName("Ivanov");
                Assert.AreEqual(1, customer.Count());
                Assert.AreEqual("Ivan", customer[0].FirstName);
                Assert.AreEqual("Ivanov", customer[0].LastName);
                Assert.AreEqual("Ivanivi@gmail.com", customer[0].Email);
                Assert.AreEqual("Lerin 21", customer[0].StreetName);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No customer with that last name.")]
        public void IfThereIsNpCustomerWithThatLastNameThrowArgumentException()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                sut.SearchByLastName("Milen");
            }
        }

        [TestMethod]
        public void SerchCustomerByParcelType()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                var parcelsList = sut.SearchByParcelType("Ivanivi@gmail.com", "preparing");
                Assert.AreEqual(1, parcelsList.Count());
                Assert.AreEqual("Ikea", parcelsList[0].OriginWarehouse);
                Assert.AreEqual("Jysk", parcelsList[0].DestinationWarehouse);
                Assert.AreEqual("10", parcelsList[0].Weight);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "This customer have no parcel with that status.")]
        public void IfThisCustomerHasNoParcelsWithThatNameThrowArgumentException()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var createAddress = new CreateAddresses(assertContext, createCity);
                var cRUDandFilterParsels = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var sut = new CRUDandSearchCustomer(assertContext, checkingValues, createAddress, cRUDandFilterParsels);

                sut.SearchByParcelType("Ivanivi@gmail.com", "completed");
            }
        }
    }
}
