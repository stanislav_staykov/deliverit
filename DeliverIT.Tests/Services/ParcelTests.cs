﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AspNetCoreDemo.Tests.Services;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class ParcelTests : TestPreparation
    {


        [TestMethod]
        public void ListingParcelForParticularPerson()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {                
                var parcel = new ParcelDTO()
                {
                    CategoryName = "Garden",
                    OriginWarehouse = "Ikea",
                    DestinationWarehouse = "Jysk",
                    Weight = "10"
                };

                List<ParcelDTO> parcels = new List<ParcelDTO> { parcel };
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var parcelsList = sut.Listing("Ivanivi@gmail.com");
                Assert.AreEqual(parcels.Count(), parcelsList.Count());
                Assert.AreEqual(parcel.CategoryName, parcelsList[0].CategoryName);
                Assert.AreEqual(parcel.OriginWarehouse, parcelsList[0].OriginWarehouse);
                Assert.AreEqual(parcel.DestinationWarehouse, parcelsList[0].DestinationWarehouse);
                Assert.AreEqual(parcel.Weight, parcelsList[0].Weight);
            }
        }

        [TestMethod]
        public void CreateParcel()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var parcel = new ParcelDTO()
                {
                    CategoryName = "Garden",
                    OriginWarehouse = "Ikea",
                    DestinationWarehouse = "Jysk",
                    Weight = "220"
                };
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                sut.Create(parcel, "Ivanivi@gmail.com");
                var createdParcel = assertContext.Parcels.Where(p => p.Id == 3).First();
                Assert.AreEqual(parcel.CategoryName, createdParcel.Category.Name);
                Assert.AreEqual(parcel.OriginWarehouse, createdParcel.Shipment.OriginWarehouse.Name);
                Assert.AreEqual(parcel.DestinationWarehouse, createdParcel.Shipment.DestinationWarehouse.Name);
                Assert.AreEqual(int.Parse(parcel.Weight), createdParcel.Weight);
            }
        }

        [TestMethod]
        public void ModifyParcel()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                sut.Modify("Ivanivi@gmail.com", 1, 150);
                var createdParcel = assertContext.Parcels.Where(p => p.Id == 1).First();
                Assert.AreEqual(1, createdParcel.Id);
                Assert.AreEqual(150, createdParcel.Weight);
            }
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No parcel with that id.")]
        public void ThrowArgumentExceptionIfThereIsNoParcelWhitThatId()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                sut.Modify("Ivanivi@gmail.com", 2, 150);
            }
        }

        [TestMethod]
        public void DeleteParcel()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var deletedParcel = sut.Delete("Ivanivi@gmail.com", 1);
                Assert.AreEqual("10", deletedParcel.Weight);
                Assert.AreEqual("Garden", deletedParcel.CategoryName);
                Assert.AreEqual("Ikea", deletedParcel.OriginWarehouse);
                Assert.AreEqual("Jysk", deletedParcel.DestinationWarehouse);
                var createdParcel = assertContext.Parcels.Where(p => p.Id == 1).FirstOrDefault();
                Assert.IsNull(createdParcel);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No parcel with that id.")]
        public void ThrowArgumentExceptionIfThereIsNoParcelWhitThatIdToBeDeleted()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                sut.Delete("Ivanivi@gmail.com", 2);
            }
        }

        [TestMethod]
        public void FilterParcelsByWeight()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var parcelInThatRange = sut.FilterByWeight(10);
                Assert.AreEqual( 1, parcelInThatRange.Count());
                Assert.AreEqual("Garden", parcelInThatRange[0].CategoryName);
                Assert.AreEqual("Ikea", parcelInThatRange[0].OriginWarehouse);
                Assert.AreEqual("Jysk", parcelInThatRange[0].DestinationWarehouse);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No parcel with that weight.")]
        public void ThrowArgumentExceptionIfThereIsNoParcelWhitThatWeight()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                sut.Delete("Ivanivi@gmail.com", 100);
            }
        }

        [TestMethod]
        public void FilterParcelsByCategory()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var parcelInThatRange = sut.FilterByCategory("Garden");
                Assert.AreEqual(2, parcelInThatRange.Count());
                Assert.AreEqual("Garden", parcelInThatRange[0].CategoryName);
                Assert.AreEqual("Ikea", parcelInThatRange[0].OriginWarehouse);
                Assert.AreEqual("Jysk", parcelInThatRange[0].DestinationWarehouse);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No parcel with that category name.")]
        public void ThrowArgumentExceptionIfThereIsNoParcelInThatCategory()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                sut.FilterByCategory("Furniture");
            }
        }

        [TestMethod]
        public void FilterParcelsByCustomer()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var parcelInThatRange = sut.FilterByCustomer("Ivan");
                Assert.AreEqual(1, parcelInThatRange.Count());
                Assert.AreEqual("Garden", parcelInThatRange[0].CategoryName);
                Assert.AreEqual("Ikea", parcelInThatRange[0].OriginWarehouse);
                Assert.AreEqual("Jysk", parcelInThatRange[0].DestinationWarehouse);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No parcel with that customer name.")]
        public void ThrowArgumentExceptionIfThereIsNoParcelsForThatPercon()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                sut.FilterByCustomer("Petur");
            }
        }

        [TestMethod]
        public void FilterParcelsByWarehouse()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                var parcelInThatRange = sut.FilterByWarehouse("Ikea");
                Assert.AreEqual(2, parcelInThatRange.Count());
                Assert.AreEqual("Garden", parcelInThatRange[0].CategoryName);
                Assert.AreEqual("Ikea", parcelInThatRange[0].OriginWarehouse);
                Assert.AreEqual("Jysk", parcelInThatRange[0].DestinationWarehouse);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No parcel with that customer name.")]
        public void ThrowArgumentExceptionIfThereIsNoParcelsInThatWarehouse()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var cRUDandFilterShipments = new CRUDandFilterShipments(assertContext, checkingValues);
                var sut = new CRUDandFilterParcels(assertContext, checkingValues, cRUDandFilterShipments);
                sut.FilterByWarehouse("Jysk");
            }
        }
    }
}
