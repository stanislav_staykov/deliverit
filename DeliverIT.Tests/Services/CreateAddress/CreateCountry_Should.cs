﻿using AspNetCoreDemo.Tests.Services;
using DeliverIT.Data;
using System.Linq;
using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DeliverIT.Tests.Services.CreateCountry
{
    [TestClass]
    public class CreateCountry_Should : TestPreparation
    {        

        [TestMethod]
        public void ReturnCreatedCountry()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var sut = new CreateCountries(assertContext);
                var country = new RegisterCustomerDTO
                {
                    Country = "Estoniq"
                };
                sut.CreateCountry(country);
                var createdCountry = assertContext.Countries.Where(c => c.Name == "Estoniq").First();
                Assert.AreEqual("Estoniq", createdCountry.Name);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException),"Insert value for country name!")]
        public void ThrowArgumentNullExceptionIfCountryNameIsNull()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var sut = new CreateCountries(assertContext);
                var country = new RegisterCustomerDTO{ };
                sut.CreateCountry(country);
            }
        }

        [TestMethod]
        public void ReturnCountryIfAlreadyExistsWithoutCreatingNewOne()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var sut = new CreateCountries(assertContext);
                var country = new RegisterCustomerDTO
                {
                    Country = "Estoniq"
                };
                sut.CreateCountry(country);
                var countryDuplicate = new RegisterCustomerDTO
                {
                    Country = "Estoniq"
                };
                sut.CreateCountry(country);
                var countryCount = assertContext.Countries.Where(c => c.Name == "Estoniq").Count();
                Assert.AreEqual(1, countryCount);
            }
        }
    }
}
