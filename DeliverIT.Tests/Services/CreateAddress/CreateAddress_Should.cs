﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AspNetCoreDemo.Tests.Services;
using DeliverIT.Data;
using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeliverIT.Tests.Services.CreateAddress
{
    [TestClass]
    public class CreateAddress_Should : TestPreparation
    {


        [TestMethod]
        public void ReturnCreatedAddress()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var address = new RegisterCustomerDTO
                {
                    StreetName = "Shustakovich 33",
                    City = "Bern",
                    Country = "Germany"
                };
                var sut = new CreateAddresses(assertContext, createCity);
                sut.CreateAddress(address);
                var createdCity = assertContext.Addresses.Where(a => a.StreetName == "Shustakovich 33").First();
                Assert.AreEqual("Shustakovich 33", createdCity.StreetName);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Insert value for street name!")]
        public void ThrowArgumentNullExceptionIfAddressNameIsNull()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var sut = new CreateAddresses(assertContext, createCity);
                var city = new RegisterCustomerDTO { };
                sut.CreateAddress(city);
            }
        }

        [TestMethod]
        public void ReturnAddressIfAlreadyExistsWithoutCreatingNewOne()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var createCountry = new CreateCountries(assertContext);
                var createCity = new CreateCities(assertContext, createCountry);
                var sut = new CreateAddresses(assertContext, createCity);
                var city = new RegisterCustomerDTO
                {
                    StreetName = "Shustakovich 33",
                    City = "Bern",
                    Country = "Germany"
                };
                sut.CreateAddress(city);

                var cityDuplicate = new RegisterCustomerDTO
                {
                    StreetName = "Shustakovich 33",
                    City = "Bern",
                    Country = "Germany"
                };
                sut.CreateAddress(cityDuplicate);

                var streetNameCount = assertContext.Addresses.Where(a => a.StreetName == "Shustakovich 33").Count();
                Assert.AreEqual(1, streetNameCount);
            }
        }
    }
}
