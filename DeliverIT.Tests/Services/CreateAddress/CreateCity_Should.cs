﻿using System;
using System.Linq;
using DeliverIT.Data;
using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using AspNetCoreDemo.Tests.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeliverIT.Tests.Services.CreateAddress
{
    [TestClass]
    public class CreateCity_Should : TestPreparation
    {
        

        [TestMethod]
        public void ReturnCreatedCity()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var createCountry = new CreateCountries(assertContext);
                var city = new RegisterCustomerDTO
                {
                    City = "Bern",
                    Country = "Germany"
                };
                var sut = new CreateCities(assertContext, createCountry);
                sut.CreateCity(city);
                var createdCity = assertContext.Cities.Where(c => c.Name == "Bern").First();
                Assert.AreEqual("Bern", createdCity.Name);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Insert value for city name!")]
        public void ThrowArgumentNullExceptionIfCityNameIsNull()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var createCountry = new CreateCountries(assertContext);
                var sut = new CreateCities(assertContext, createCountry);
                var city = new RegisterCustomerDTO { };
                sut.CreateCity(city);
            }
        }

        [TestMethod]
        public void ReturnCityIfAlreadyExistsWithoutCreatingNewOne()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var createCountry = new CreateCountries(assertContext);
                var sut = new CreateCities(assertContext, createCountry);
                var city = new RegisterCustomerDTO
                {
                    City = "Bern",
                    Country = "Germany"
                };
                sut.CreateCity(city);

                var cityDuplicate = new RegisterCustomerDTO
                {
                    City = "Bern",
                    Country = "Germany"
                };
                sut.CreateCity(cityDuplicate);

                var cityCount = assertContext.Cities.Where(c => c.Name == "Bern").Count();
                Assert.AreEqual(1, cityCount);
            }
        }
    }
}
