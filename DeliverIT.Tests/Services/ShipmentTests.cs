﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AspNetCoreDemo.Tests.Services;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services;
using DeliverIT.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class ShipmentTests : TestPreparation
    {


        [TestMethod]
        public void ListingShipmentsForParticularPerson()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var shipment = new ShipmentDTO()
                {
                    OriginWarehouse = "Ikea",
                    DestinationWarehouse = "Jysk"
                };
                List<ShipmentDTO> shipments = new List<ShipmentDTO> { shipment };
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                var shipmentsInBase = sut.Listing("Ivanivi@gmail.com");
                Assert.AreEqual(shipments.Count(), shipmentsInBase.Count());
                Assert.AreEqual(shipments[0].OriginWarehouse, shipmentsInBase[0].OriginWarehouse);
                Assert.AreEqual(shipments[0].DestinationWarehouse, shipmentsInBase[0].DestinationWarehouse);
            }
        }

        [TestMethod]
        public void CreateShipment()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var parcel = new ParcelDTO()
                {
                    CategoryName = "Garden",
                    OriginWarehouse = "Ikea",
                    DestinationWarehouse = "Jysk",
                    Weight = "220"
                };
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                var createdShipment = sut.Create(parcel.OriginWarehouse, parcel.DestinationWarehouse, "Ivanivi@gmail.com");                
                Assert.AreEqual(parcel.OriginWarehouse, assertContext.Warehouses.Where(ow => ow.Id == createdShipment.OriginWarehouseId)
                                                                                .ToList()
                                                                                .Select(w => w.Name)
                                                                                .First());
                Assert.AreEqual(parcel.DestinationWarehouse, assertContext.Warehouses.Where(ow => ow.Id == createdShipment.DestinationWarehouseId)
                                                                                     .ToList()
                                                                                     .Select(w => w.Name)
                                                                                     .First());
            }
        }

        [TestMethod]
        public void ModifyShipment()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                sut.Modify("Ivanivi@gmail.com", 1, 1);
                var createdShipment = assertContext.Shipments.Where(s => s.Id == 1).First();
                Assert.AreEqual("Ikea", createdShipment.OriginWarehouse.Name);
                Assert.AreEqual("Ikea", createdShipment.DestinationWarehouse.Name);
            }
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No origin warehouse with that id.")]
        public void ThrowArgumentExceptionIfThereIsNoShipmentWhitThatId()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                sut.Modify("Ivanivi@gmail.com", 3, 1);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No destination warehouse with that id.")]
        public void ThrowArgumentExceptionIfThereIsNoDestinationWarehouseWhitThatId()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                sut.Modify("Ivanivi@gmail.com", 1, 3);
            }
        }

        [TestMethod]
        public void DeleteShipment()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                var deletedShipment = sut.Delete("Ivanivi@gmail.com", 1);
                Assert.AreEqual("Ikea", deletedShipment.OriginWarehouse);
                Assert.AreEqual("Jysk", deletedShipment.DestinationWarehouse);
                var delShipment = assertContext.Shipments.Where(s => s.Id == 1).FirstOrDefault();
                Assert.IsNull(delShipment);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No shipment with that id.")]
        public void ThrowArgumentExceptionIfThereIsNoShipmentWhitThatIdToBeDeleted()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                var deletedShipment = sut.Delete("Ivanivi@gmail.com", 3);
            }
        }

        [TestMethod]
        public void FilterShipmentByCustomer()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                var shipmentsInThatRange = sut.FilterPerson("Ivan");
                Assert.AreEqual(1, shipmentsInThatRange.Count());
                Assert.AreEqual("Ikea", shipmentsInThatRange[0].OriginWarehouse);
                Assert.AreEqual("Jysk", shipmentsInThatRange[0].DestinationWarehouse);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No shipment for that person.")]
        public void ThrowArgumentExceptionIfThereIsNoShipmentsForThatPerson()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                var shipmentsInThatRange = sut.FilterPerson("Petur");
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No person with that name.")]
        public void ThrowArgumentExceptionIfThereIsNoPrsonWhitThatName()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                var shipmentsInThatRange = sut.FilterPerson("Petri");
            }
        }

        [TestMethod]
        public void FilterShipmentsByWarehouse()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                var shipmentsInThatRange = sut.Filter("Ikea");
                Assert.AreEqual(1, shipmentsInThatRange.Count());
                Assert.AreEqual("Ikea", shipmentsInThatRange[0].OriginWarehouse);
                Assert.AreEqual("Jysk", shipmentsInThatRange[0].DestinationWarehouse);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No parcel with that customer name.")]
        public void ThrowArgumentExceptionIfThereIsNoWarehouseWhitThatName()
        {
            // Act & Assert
            using(var assertContext = new DeliverItContext(options))
            {
                var checkingValues = new CheckingValues(assertContext);
                var sut = new CRUDandFilterShipments(assertContext, checkingValues);
                var shipmentsInThatRange = sut.Filter("Keke");
            }
        }
    }
}
