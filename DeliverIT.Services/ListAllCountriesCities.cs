﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Services
{
    public class ListAllCountriesCities : IListAllCountriesCities
    {
        private DeliverItContext dbContext;

        public ListAllCountriesCities(DeliverItContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public List<CountriesDTO> ListingCountries()
        {
            List<CountriesDTO> result = this.dbContext.Countries.Select(c => new CountriesDTO(c) { })
                                                                .ToList();

            return result;
        }

        public List<Country> ListingCountriesFotWeb()
        {
            List<Country> result = this.dbContext.Countries.ToList();

            return result;
        }

        public CountriesDTO GetCountryById(int id)
        {
            CountriesDTO result = this.dbContext.Countries.Where(c => c.Id == id).Select(c => new CountriesDTO(c) { }).First();

            return result;
        }

        public List<CitiesDTO> ListingCities()
        {
            List<CitiesDTO> result = this.dbContext.Cities.Include(c => c.Country).Select(c => new CitiesDTO(c) { })
                                                                .ToList();

            return result;
        }

        public List<City> ListingCitiesForWeb()
        {
            List<City> result = this.dbContext.Cities.Include(c => c.Country)
                                                     .ToList();

            return result;
        }

        public CitiesDTO GetCityById(int id)
        {
            CitiesDTO result = this.dbContext.Cities.Include(c => c.Country).Where(c => c.Id == id).Select(c => new CitiesDTO(c) { }).First();

            return result;
        } 

    }
}
