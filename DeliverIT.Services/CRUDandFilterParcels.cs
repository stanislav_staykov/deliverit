﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Services
{
    public class CRUDandFilterParcels : ICRUDandFilterParcels
    {
        private readonly DeliverItContext dbContext;
        private readonly ICheckingValues checkingValues;
        private readonly ICRUDandFilterShipments listCreateModifyShipments;

        public CRUDandFilterParcels(DeliverItContext dbContext,
                                       ICheckingValues checkingValues,
                                       ICRUDandFilterShipments listCreateModifyShipments)
        {
            this.dbContext = dbContext;
            this.checkingValues = checkingValues;
            this.listCreateModifyShipments = listCreateModifyShipments;
        }
        public List<ParcelDTO> Listing(string email)
        {
            int id = this.checkingValues.FindIdViaEmail(email);
            List<ParcelDTO> result = this.dbContext.Parcels.Include(p => p.Category)
                                                           .Include(p => p.Shipment.OriginWarehouse)
                                                           .Include(p => p.Shipment.DestinationWarehouse)
                                                           .Include(p => p.Shipment)
                                                           .Where(p => p.CustomerId == id)
                                                           .ToList().Select(p => new ParcelDTO(p) { })
                                                           .ToList();

            return result;
        }


        public List<Parcel> ListingParcels()
        {
            //int id = this.checkingValues.FindIdViaEmail(email);
            List<Parcel> result = this.dbContext.Parcels.Include(p => p.Category)
                                                           .Include(p => p.Shipment.OriginWarehouse)
                                                           .Include(p => p.Shipment.DestinationWarehouse)
                                                           .Include(p => p.Shipment)
                                                           .ToList();

            return result;
        }

        public Parcel GetParcelById(int id)
        {
            var result = this.dbContext.Parcels.Include(p => p.Category)
                                                           .Include(p => p.Shipment.OriginWarehouse)
                                                           .Include(p => p.Shipment.DestinationWarehouse)
                                                           .Include(p => p.Shipment)
                                                           .Where(p => p.Id == id)
                                                           .First();

            return result;
        }

        public IEnumerable<Category> GetAllCategories()
        {
            var result = this.dbContext.Categories;

            return result;
        }

        public ParcelDTO Create(ParcelDTO parcelData, string email)
        {
            var createParsel = new Parcel();
            createParsel.CategoryId = this.checkingValues.CategoryCheck(parcelData.CategoryName);
            createParsel.CustomerId = this.checkingValues.FindIdViaEmail(email);
            var shipments = this.listCreateModifyShipments.Create(parcelData.OriginWarehouse, parcelData.DestinationWarehouse, email);
            createParsel.WarehouseId = shipments.OriginWarehouseId;
            createParsel.ShipmentId = shipments.Id;
            createParsel.Weight = this.checkingValues.WeightCheck(parcelData.Weight);
            this.dbContext.Parcels.Add(createParsel);
            this.dbContext.SaveChanges();
            ParcelDTO parcel = this.dbContext.Parcels.Include(p => p.Category)
                                                     .Include(p => p.Shipment)
                                                     .ThenInclude(s => s.OriginWarehouse)
                                                     .Include(s => s.Shipment.DestinationWarehouse)
                                                     .OrderByDescending(p => p.Id)
                                                     .Select(p => new ParcelDTO(p) { })
                                                     .First();

            return parcel;
        }

        public ParcelDTO Modify(string email, int id, double weight)
        {
            int idCustomer = this.checkingValues.FindIdViaEmail(email);
            var parcelModify = dbContext.Parcels.Include(p => p.Category)
                                                .Include(p => p.Shipment)
                                                .ThenInclude(s => s.OriginWarehouse)
                                                .Include(s => s.Shipment.DestinationWarehouse)
                                                .Where(p => p.CustomerId == idCustomer)
                                                .FirstOrDefault(p => p.Id == id);
            if(parcelModify != null)
            {
                parcelModify.Weight = weight;
                dbContext.SaveChanges();
                ParcelDTO result = new ParcelDTO(parcelModify);

                return result;
            }
            throw new ArgumentException("No parcel with that id.");
        }

        public ParcelDTO Delete(string email, int id)
        {
            int idCustomer = this.checkingValues.FindIdViaEmail(email);
            Parcel parcel = dbContext.Parcels.Include(p => p.Shipment)
                                             .ThenInclude(s => s.OriginWarehouse)
                                             .Include(s => s.Shipment.DestinationWarehouse)
                                             .Include(p => p.Category)
                                             .Include(p => p.Warehouse)
                                             .Where(p => p.CustomerId == idCustomer)
                                             .FirstOrDefault(p => p.Id == id);

            if(parcel != null)
            {
                dbContext.Remove(parcel);
                dbContext.SaveChanges();
                ParcelDTO result = new ParcelDTO(parcel);

                return result;
            }
            throw new ArgumentException("No parcel with that id.");
        }

        public List<ParcelDTO> FilterByWeight(double weight)
        {
            if(this.dbContext.Parcels.FirstOrDefault(p => p.Weight == weight) != null)
            {
                List<ParcelDTO> result = this.dbContext.Parcels.Include(p => p.Shipment)
                                                               .ThenInclude(s => s.OriginWarehouse)
                                                               .Include(s => s.Shipment.DestinationWarehouse)
                                                               .Include(p => p.Category)
                                                               .Include(p => p.Warehouse)
                                                               .Where(p => p.Weight == weight)
                                                               .ToList().Select(p => new ParcelDTO(p) { })
                                                               .ToList();

                return result;
            }
            throw new ArgumentException("No parcel with that weight.");
        }

        public List<ParcelDTO> FilterByCustomer(string customerName)
        {
            if(this.dbContext.Parcels.Include(p => p.Customer).FirstOrDefault(p => p.Customer.FirstName == customerName) != null)
            {
                List<ParcelDTO> result = this.dbContext.Parcels.Include(p => p.Customer)
                                                               .Include(p => p.Shipment)
                                                               .ThenInclude(s => s.OriginWarehouse)
                                                               .Include(s => s.Shipment.DestinationWarehouse)
                                                               .Include(p => p.Category)
                                                               .Include(p => p.Warehouse)
                                                               .Where(p => p.Customer.FirstName == customerName)
                                                               .ToList().Select(p => new ParcelDTO(p) { })
                                                               .ToList();

                return result;
            }
            throw new ArgumentException("No parcel with that customer name.");
        }

        public List<ParcelDTO> FilterByWarehouse(string warehouseName)
        {
            if(this.dbContext.Parcels.Include(p => p.Warehouse).FirstOrDefault(p => p.Warehouse.Name == warehouseName) != null)
            {
                List<ParcelDTO> result = this.dbContext.Parcels.Include(p => p.Customer)
                                                               .Include(p => p.Warehouse)
                                                               .Include(p => p.Shipment)
                                                               .ThenInclude(p => p.OriginWarehouse)
                                                               .Include(p => p.Shipment.DestinationWarehouse)
                                                               .Include(p => p.Category)
                                                               .Where(p => p.Warehouse.Name == warehouseName)
                                                               .ToList().Select(p => new ParcelDTO(p) { })
                                                               .ToList();

                return result;
            }
            throw new ArgumentException("No parcel with that warehouse name.");
        }

        public List<ParcelDTO> FilterByCategory(string categoryName)
        {
            if(this.dbContext.Parcels.Include(p => p.Category).FirstOrDefault(p => p.Category.Name == categoryName) != null)
            {
                List<ParcelDTO> result = this.dbContext.Parcels.Include(p => p.Customer)
                                                               .Include(p => p.Shipment)
                                                               .ThenInclude(s => s.OriginWarehouse)
                                                               .Include(s => s.Shipment.DestinationWarehouse)
                                                               .Include(p => p.Category)
                                                               .Include(p => p.Warehouse)
                                                               .Where(p => p.Category.Name == categoryName)
                                                               .ToList().Select(p => new ParcelDTO(p) { })
                                                               .ToList();

                return result;
            }
            throw new ArgumentException("No parcel with that category name.");
        }
    }
}
