﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services
{
    public class CreateCities : ICreateCities
    {
        private readonly DeliverItContext dbContext;
        private readonly ICreateCountries createCountries;

        public CreateCities(DeliverItContext dbContext, ICreateCountries createCountries)
        {
            this.dbContext = dbContext;
            this.createCountries = createCountries;
        }

        public int CreateCity(ICity cityData)
        {
            int id;
            if(cityData.City == null)
            {
                throw new ArgumentNullException("Insert value for city name!");
            }
            var cityName = this.dbContext.Cities.FirstOrDefault(c => c.Name == cityData.City);
            if(cityName != null)
            {
                id = cityName.Id;
            }
            else
            {
                var city = new City();
                city.Name = cityData.City;
                city.CountryId = this.createCountries.CreateCountry(cityData);
                this.dbContext.Cities.Add(city);
                this.dbContext.SaveChanges();
                id = this.dbContext.Cities.Where(c => c.Name == cityData.City).Select(c => c.Id).First();
            }
            return id;
        }
    }
}
