﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Services
{
    public class CRUDandFilterShipments : ICRUDandFilterShipments
    {
        private readonly DeliverItContext dbContext;
        private readonly ICheckingValues checkingValues;

        public CRUDandFilterShipments(DeliverItContext dbContext,
                                       ICheckingValues checkingValues)
        {
            this.dbContext = dbContext;
            this.checkingValues = checkingValues;
        }
        public List<ShipmentDTO> Listing(string email)
        {
            int id = this.checkingValues.FindIdViaEmail(email);
            List<ShipmentDTO> result = this.dbContext.Parcels
                .Include(p => p.Shipment)
                .ThenInclude(s => s.OriginWarehouse)
                .Include(s => s.Shipment.DestinationWarehouse)
                .Where(s => s.CustomerId == id)
                .ToList().Select(p => new ShipmentDTO(p) { })
                .ToList();

            return result;
        }

        public List<Shipment> Listing()
        {
            var result = this.dbContext.Shipments.Include(s => s.OriginWarehouse)
                                                 .Include(s => s.DestinationWarehouse)
                                                 .ToList();

            return result;
        }

        public Shipment GetShipmentById(int id)
        {
            var result = this.dbContext.Shipments
                .Include(s => s.OriginWarehouse)
                .Include(s => s.DestinationWarehouse)
                .Where(s => s.Id == id)
                .First();

            return result;
        }

        public List<Warehouse> GetAllWarehouses()
        {
            var result = this.dbContext.Warehouses.ToList();
                
            return result;
        }

        public Shipment Create(string originWarehouse, string destinationWarehouse, string email)
        {
            var createShipment = new Shipment();
            createShipment.Departure = DateTime.Now;
            createShipment.Arrival = createShipment.Departure.AddDays(10);
            createShipment.Status = Data.Enum.Status.preparing;
            createShipment.OriginWarehouseId = this.checkingValues.WerehouseCheck(originWarehouse);
            createShipment.DestinationWarehouseId = this.checkingValues.WerehouseCheck(destinationWarehouse);
            this.dbContext.Shipments.Add(createShipment);
            this.dbContext.SaveChanges();

            return createShipment;
        }
        public ShipmentDTO Modify(string email, int id, int destinationWherehouseId)
        {
            int idCustomer = this.checkingValues.FindIdViaEmail(email);
            var shipmentModify = dbContext.Parcels.Include(p => p.Shipment)
                                                .ThenInclude(s => s.OriginWarehouse)
                                                .Include(p => p.Shipment.DestinationWarehouse)
                                                .Where(p => p.CustomerId == idCustomer)
                                                .FirstOrDefault(p => p.Shipment.OriginWarehouseId == id);
            if(shipmentModify != null)
            {
                if(dbContext.Warehouses.FirstOrDefault(w => w.Id == destinationWherehouseId) != null)
                {
                    shipmentModify.Shipment.DestinationWarehouseId = destinationWherehouseId;
                    dbContext.SaveChanges();
                    ShipmentDTO result = new ShipmentDTO(shipmentModify);

                    return result;
                }
                else
                {
                    throw new ArgumentException("No destination warehouse with that id.");
                }

            }
            throw new ArgumentException("No origin warehouse with that id.");
        }

        public ShipmentDTO Modify(string email, int id, string destinationWherehouse)
        {
            int idCustomer = this.checkingValues.FindIdViaEmail(email);
            var shipmentModify = dbContext.Parcels.Include(p => p.Shipment)
                                                .ThenInclude(s => s.OriginWarehouse)
                                                .Include(p => p.Shipment.DestinationWarehouse)
                                                .Where(p => p.CustomerId == idCustomer)
                                                .FirstOrDefault(p => p.Shipment.Id == id);
            if(shipmentModify != null)
            {
                var warehouse = dbContext.Warehouses.FirstOrDefault(w => w.Name == destinationWherehouse);
                if(warehouse != null)
                {
                    shipmentModify.Shipment.DestinationWarehouseId = warehouse.Id;
                    dbContext.SaveChanges();
                    ShipmentDTO result = new ShipmentDTO(shipmentModify);

                    return result;
                }
                else
                {
                    throw new ArgumentException("No destination warehouse with that name.");
                }

            }
            throw new ArgumentException("No origin warehouse with that id.");
        }

        public ShipmentDTO Delete(string email, int id)
        {
            int idCustomer = this.checkingValues.FindIdViaEmail(email);
            Shipment shipment = dbContext.Shipments.Include(s => s.OriginWarehouse)
                                                       .Include(s => s.DestinationWarehouse)
                                                       .FirstOrDefault(p => p.Id == id);

            if(shipment != null)
            {
                dbContext.Remove(shipment);
                dbContext.SaveChanges();
                ShipmentDTO result = new ShipmentDTO(shipment);

                return result;
            }
            throw new ArgumentException("No shipment with that id.");
        }

        public List<ShipmentDTO> Filter(string warehouseName)
        {
            if(this.dbContext.Warehouses.FirstOrDefault(w => w.Name == warehouseName) != null)
            {
                List<ShipmentDTO> result = this.dbContext.Parcels
                .Include(p => p.Shipment)
                .ThenInclude(s => s.OriginWarehouse)
                .Include(s => s.Shipment.DestinationWarehouse)
                .Where(s => s.Shipment.OriginWarehouse.Name == warehouseName)
                .ToList().Select(p => new ShipmentDTO(p) { })
                .ToList();

                return result;
            }
            throw new ArgumentException("No shipment with that name.");
        }

        public List<ShipmentDTO> FilterPerson(string personName)
        {
            if(this.dbContext.Customers.FirstOrDefault(w => w.FirstName == personName) != null)
            {
                int id = this.dbContext.Customers.Where(c => c.FirstName == personName).Select(c => c.Id).First();
                List<ShipmentDTO> result = this.dbContext.Parcels
                                               .Include(p => p.Shipment)
                                               .ThenInclude(s => s.OriginWarehouse)
                                               .Include(s => s.Shipment.DestinationWarehouse)
                                               .Where(s => s.CustomerId == id)
                                               .ToList().Select(p => new ShipmentDTO(p) { })
                                               .ToList();
                if(result.Count() == 0)
                {
                    throw new ArgumentException("No shipment for that person.");
                }
                return result;
            }
            throw new ArgumentException("No person with that name.");
        }
    }
}
