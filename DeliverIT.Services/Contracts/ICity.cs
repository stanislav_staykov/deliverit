﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface ICity : ICountry
    {
        public string City { get; set; }
    }
}
