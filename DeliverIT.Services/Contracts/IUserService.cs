﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Services.Enum;

namespace DeliverIT.Services.Contracts
{
    public interface IUserService
    {
        public Access? Get(string email);
        public bool GetEmail(string email);
        public string GetUserName(string email);
    }
}
