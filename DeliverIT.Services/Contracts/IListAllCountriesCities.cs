﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Contracts
{
    public interface IListAllCountriesCities
    {
        public List<CountriesDTO> ListingCountries();
        public List<Country> ListingCountriesFotWeb();
        public CountriesDTO GetCountryById(int id);
        public List<CitiesDTO> ListingCities();
        public List<City> ListingCitiesForWeb();
        public CitiesDTO GetCityById(int id);
    }
}
