﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Enum;

namespace DeliverIT.Services.Contracts
{
    public interface ICheckingValues
    {
        public bool WerehouseCheck(int id);
        public int CategoryCheck(string name);
        public int FindIdViaEmail(string email);
        public double WeightCheck(string weight);
        public int WerehouseCheck(string Name);
        public DateTime ValidateDateTime(string dateTime);
        public Status CheckStatus(string status);
        public bool CheckWarehouse(string name);
    }
}
