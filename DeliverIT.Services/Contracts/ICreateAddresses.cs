﻿using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Contracts
{
    public interface ICreateAddresses
    {
        public int CreateAddress(IAddress addressData);
    }
}
