﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IAddress : ICity
    {
        public string StreetName { get; set; }
    }
}
