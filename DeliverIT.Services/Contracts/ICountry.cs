﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface ICountry
    {
        public string Country { get; set; }
    }
}
