﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Contracts
{
    public interface ICRUDandSearchCustomer
    {
        public CustomerDTO Get(string name);
        public List<WarehouseDTO> GetAllWarehouses();
        public List<CustomerDTO> Listing();
        public string Create(RegisterCustomerDTO customerdata);
        public CustomerDTO Modify(string email, string newEmail);
        public CustomerDTO Delete(string email);
        public CustomerDTO SerchByEmail(string email);
        public List<CustomerDTO> SearchByFirstName(string fName);
        public List<CustomerDTO> SearchByLastName(string lName);
        public List<ParcelDTO> SearchByParcelType(string email, string type);
    }
}
