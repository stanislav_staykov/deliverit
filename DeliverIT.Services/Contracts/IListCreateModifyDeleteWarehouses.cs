﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Contracts
{
    public interface IListCreateModifyDeleteWarehouses
    {
        public List<WarehouseDTO> Listing();
        public List<Warehouse> ListingWarehouse();
        public Warehouse ListingWarehouseById(int id);
        public IEnumerable<Address> GetAllAddresses();
        public WarehouseDTO Create(WarehouseDTO warehouseData);
        public WarehouseDTO Modify(string name, string newName);
    }
}
