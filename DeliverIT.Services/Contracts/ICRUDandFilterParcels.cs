﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Contracts
{
    public interface ICRUDandFilterParcels
    {
        public List<ParcelDTO> Listing(string email);
        public List<Parcel> ListingParcels();
        public IEnumerable<Category> GetAllCategories();
        public ParcelDTO Create(ParcelDTO parcelData, string email);
        public ParcelDTO Modify(string email, int id, double weight);
        public Parcel GetParcelById(int id);
        public ParcelDTO Delete(string email, int id);
        public List<ParcelDTO> FilterByWeight(double weight);
        public List<ParcelDTO> FilterByCustomer(string customerName);
        public List<ParcelDTO> FilterByWarehouse(string warehouseName);
        public List<ParcelDTO> FilterByCategory(string categoryName);
    }
}
