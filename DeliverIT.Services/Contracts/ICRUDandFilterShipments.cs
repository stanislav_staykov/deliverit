﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Contracts
{
    public interface ICRUDandFilterShipments
    {
        public List<ShipmentDTO> Listing(string email);
        public List<Shipment> Listing();
        public Shipment GetShipmentById(int id);
        public List<Warehouse> GetAllWarehouses();
        public Shipment Create(string originWarehouse, string destinationWarehouse, string email);
        public ShipmentDTO Modify(string email, int id, int destinationWherehouseId);
        public ShipmentDTO Modify(string email, int id, string destinationWherehouse);
        public ShipmentDTO Delete(string email, int id);
        public List<ShipmentDTO> Filter(string warehouseName);
        public List<ShipmentDTO> FilterPerson(string personName);

    }
}
