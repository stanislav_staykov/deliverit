﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Contracts
{
    public interface ICreateCountries
    {
        public int CreateCountry(ICountry country);
    }
}
