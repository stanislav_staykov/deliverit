﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services
{
    public class CreateAddresses : ICreateAddresses
    {
        private readonly DeliverItContext dbContext;
        private readonly ICreateCities createCities;

        public CreateAddresses(DeliverItContext dbContext, ICreateCities createCities)
        {
            this.dbContext = dbContext;
            this.createCities = createCities;
        }

        public int CreateAddress(IAddress addressData)
        {
            int id;
            if(addressData.StreetName == null)
            {
                throw new ArgumentNullException("Insert value for street name!");
            }
            var streetName = this.dbContext.Addresses.FirstOrDefault(a => a.StreetName == addressData.StreetName);
            if(streetName != null)
            {
                id = streetName.Id;
            }
            else
            {
                var address = new Address();
                address.StreetName = addressData.StreetName;
                address.CityID = this.createCities.CreateCity(addressData);
                this.dbContext.Addresses.Add(address);
                this.dbContext.SaveChanges();
                id = this.dbContext.Addresses.Where(c => c.StreetName == addressData.StreetName).Select(c => c.Id).First();
            }

            return id;
        }
    }
}
