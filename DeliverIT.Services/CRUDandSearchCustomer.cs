﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data;
using DeliverIT.Data.Enum;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Services
{
    public class CRUDandSearchCustomer : ICRUDandSearchCustomer
    {
        private readonly DeliverItContext dbContext;
        private readonly ICheckingValues checkingValues;
        private readonly ICreateAddresses createAddress;
        private readonly ICRUDandFilterParcels cRUDandFilterParsels;

        public CRUDandSearchCustomer(DeliverItContext dbContext,
                                       ICheckingValues checkingValues,
                                       ICreateAddresses createAddress,
                                       ICRUDandFilterParcels cRUDandFilterParsels)
        {
            this.dbContext = dbContext;
            this.checkingValues = checkingValues;
            this.createAddress = createAddress;
            this.cRUDandFilterParsels = cRUDandFilterParsels;
        }

        public CustomerDTO Get(string name)
        {
            Customer result = this.dbContext.Customers.Include(a => a.Address).FirstOrDefault(n => n.FirstName == name);
            if(result == null)
            {
                throw new ArgumentNullException($"Thare is no customer with name {name}.");
            }
            CustomerDTO customer = new CustomerDTO(result);

            return customer;
        }

        public List<WarehouseDTO> GetAllWarehouses()
        {
            List<WarehouseDTO> result = this.dbContext.Warehouses
               .Include(w => w.Address)
               .ThenInclude(a => a.City)
               .ThenInclude(c => c.Country)
               .ToList().Select(c => new WarehouseDTO(c) { })
               .ToList();

            return result;
        }

        public List<CustomerDTO> Listing()
        {
            List<CustomerDTO> result = this.dbContext.Customers.Include(c => c.Address)
                                                               .ToList()
                                                               .Select(c => new CustomerDTO(c) { })
                                                               .ToList();

            return result;
        }

        public string Create(RegisterCustomerDTO customerdata)
        {
            var cutomer = new Customer();
            cutomer.FirstName = customerdata.FirstName.Length > 2 ? customerdata.FirstName : throw new ArgumentException("Insert correct value for FirstName!");
            cutomer.LastName = customerdata.LastName.Length > 2 ? customerdata.LastName : throw new ArgumentException("Insert correct value for LastName!");
            cutomer.Email = customerdata.Email;
            cutomer.AddressId = this.createAddress.CreateAddress(customerdata);
            this.dbContext.Customers.Add(cutomer);
            this.dbContext.SaveChanges();
            return customerdata.Email;
        }
        public CustomerDTO Modify(string email, string newEmail)
        {
            int idCustomer = this.checkingValues.FindIdViaEmail(email);
            var customerModify = this.dbContext.Customers.Include(c => c.Address)
                                                         .ThenInclude(a => a.City)
                                                         .ThenInclude(c => c.Country)
                                                         .Where(c => c.Email == email)
                                                         .First();
            if(this.dbContext.Customers.Where(c => c.Email == newEmail).FirstOrDefault() == null)
            {
                customerModify.Email = newEmail;
                dbContext.SaveChanges();
                CustomerDTO result = new CustomerDTO(customerModify);

                return result;
            }
            throw new ArgumentException("Customer with that email exist.");
        }

        public CustomerDTO Delete(string email)
        {
            int idCustomer = this.checkingValues.FindIdViaEmail(email);
            dbContext.Parcels.Include(p => p.Customer)
                             .ThenInclude(c => c.Address)
                             .ThenInclude(a => a.City)
                             .ThenInclude(c => c.Country)
                             .Where(p => p.Customer.Email == email)
                             .ToList()
                             .Select(p => this.cRUDandFilterParsels
                             .Delete(email, p.Id));
            Customer customer = this.dbContext.Customers.Include(c => c.Address)
                                                        .ThenInclude(a => a.City)
                                                        .ThenInclude(c => c.Country)
                                                        .Where(c => c.Email == email)
                                                        .First();
            if(customer != null)
            {
                dbContext.Remove(customer);
                dbContext.SaveChanges();
                CustomerDTO result = new CustomerDTO(customer);

                return result;
            }
            throw new ArgumentException("No customer with that email.");
        }

        public CustomerDTO SerchByEmail(string email)
        {
            int id = this.checkingValues.FindIdViaEmail(email);
            CustomerDTO result = this.dbContext.Customers.Include(c => c.Address)
                                                         .ThenInclude(a => a.City)
                                                         .ThenInclude(c => c.Country)
                                                         .Where(c => c.Id == id)
                                                         .ToList()
                                                         .Select(c => new CustomerDTO(c))
                                                         .First();

            return result;
        }

        public List<CustomerDTO> SearchByFirstName(string fName)
        {
            List<CustomerDTO> result = this.dbContext.Customers.Include(c => c.Address)
                                                               .ThenInclude(a => a.City)
                                                               .ThenInclude(c => c.Country)
                                                               .Where(c => c.FirstName == fName)
                                                               .ToList()
                                                               .Select(c => new CustomerDTO(c))
                                                               .ToList();
            if(result.Count() == 0)
            {
                throw new ArgumentException("No customer with that first name.");
            }

            return result;
        }

        public List<CustomerDTO> SearchByLastName(string lName)
        {
            List<CustomerDTO> result = this.dbContext.Customers.Include(c => c.Address)
                                                               .ThenInclude(a => a.City)
                                                               .ThenInclude(c => c.Country)
                                                               .Where(c => c.LastName == lName)
                                                               .ToList()
                                                               .Select(c => new CustomerDTO(c))
                                                               .ToList();
            if(result.Count() == 0)
            {
                throw new ArgumentException("No customer with that last name.");
            }

            return result;
        }

        public List<ParcelDTO> SearchByParcelType(string email, string type)
        {
            Status status = this.checkingValues.CheckStatus(type);
            int id = this.checkingValues.FindIdViaEmail(email);
            List<ParcelDTO> result = this.dbContext.Parcels.Include(p => p.Shipment)
                                                           .ThenInclude(s => s.OriginWarehouse)
                                                           .Include(p => p.Shipment.DestinationWarehouse)
                                                           .Include(p => p.Category)
                                                           .Include(p => p.Warehouse)
                                                           .Where(p => p.CustomerId == id)
                                                           .Where(p => p.Shipment.Status == status)
                                                           .ToList()
                                                           .Select(p => new ParcelDTO(p))
                                                           .ToList();
            if(result.Count() == 0)
            {
                throw new ArgumentException("This customer have no parcel with that status.");
            }

            return result;
        }
    }
}
