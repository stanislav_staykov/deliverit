﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;

namespace DeliverIT.Services.DTOs
{
    public class WarehouseDTO : IAddress
    {
        public WarehouseDTO()
        {

        }
        public WarehouseDTO(Warehouse warehouse)
        {
            this.Warehouse = warehouse.Name;
            this.StreetName = warehouse.Address.StreetName;
            this.City = warehouse.Address.City.Name;
            this.Country = warehouse.Address.City.Country.Name;
        }
        public string Warehouse { get; set; }
        public string StreetName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
