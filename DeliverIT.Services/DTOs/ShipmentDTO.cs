﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Enum;
using DeliverIT.Data.Models;

namespace DeliverIT.Services.DTOs
{
    public class ShipmentDTO
    {
        public ShipmentDTO()        {

        }
        public ShipmentDTO(Shipment shipment)
        {
            this.Departure = shipment.Departure.ToString();
            this.Arrival = shipment.Arrival.ToString();
            this.Status = shipment.Status.ToString();
            this.OriginWarehouse = shipment.OriginWarehouse.Name;
            this.DestinationWarehouse = shipment.DestinationWarehouse.Name;
        }
        public ShipmentDTO(Parcel parcel)
        {
            this.Departure = parcel.Shipment.Departure.ToString();
            this.Arrival = parcel.Shipment.Arrival.ToString();
            this.Status = parcel.Shipment.Status.ToString();
            this.OriginWarehouse = parcel.Shipment.OriginWarehouse.Name;
            this.DestinationWarehouse = parcel.Shipment.DestinationWarehouse.Name;
        }
        public string Departure { get; set; }
        public string Arrival { get; set; }
        public string Status { get; set; }
        public string OriginWarehouse { get; set; }
        public string DestinationWarehouse { get; set; }
    }
}
