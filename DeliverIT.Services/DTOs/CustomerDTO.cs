﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Models;

namespace DeliverIT.Services.DTOs
{
    public class CustomerDTO
    {
        public CustomerDTO()
        {

        }
        public CustomerDTO( Customer customer)
        {
            this.FirstName = customer.FirstName;
            this.LastName = customer.LastName;
            this.Email = customer.Email;
            this.StreetName = customer.Address.StreetName;
        }
        public string FirstName { get; set; }
        public string  LastName { get; set; }
        public string Email { get; set; }
        public string  StreetName { get; set; }
    }
}
