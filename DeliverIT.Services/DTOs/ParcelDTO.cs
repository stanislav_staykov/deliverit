﻿using System;
using DeliverIT.Data.Models;

namespace DeliverIT.Services.DTOs
{
    public class ParcelDTO
    {
        public ParcelDTO()
        {

        }
        public ParcelDTO(Parcel parcel )
        {           
            this.CategoryName = parcel.Category.Name;
            this.Weight = parcel.Weight.ToString();
            this.OriginWarehouse = parcel.Shipment.OriginWarehouse.Name;
            this.DestinationWarehouse = parcel.Shipment.DestinationWarehouse.Name;
        }

        public string CategoryName { get; set; }
        public string Weight { get; set; }
        public string OriginWarehouse { get; set; }      
        public string DestinationWarehouse { get; set; }
    }
}
