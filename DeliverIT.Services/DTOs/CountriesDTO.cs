﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;

namespace DeliverIT.Services.DTOs
{
    public class CountriesDTO : ICountry
    {
        public CountriesDTO()
        {

        }

        public CountriesDTO(Country country)
        {
            this.Country = country.Name;
        }
        public string Country { get; set; }
    }
}
