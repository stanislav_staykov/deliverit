﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Services.Contracts;

namespace DeliverIT.Services.DTOs
{
    public class RegisterCustomerDTO : IAddress
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string StreetName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
