﻿using System;
using System.Collections.Generic;
using System.Text;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;

namespace DeliverIT.Services.DTOs
{
    public class CitiesDTO : ICity
    {
        public CitiesDTO()
        {

        }

        public CitiesDTO(City city)
        {
            this.City = city.Name;
            this.Country = city.Country.Name;
        }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
