﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Enum;

namespace DeliverIT.Services
{
    public class UserService : IUserService
    {
        private readonly DeliverItContext dbContext;

        public UserService(DeliverItContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public Access? Get(string email)
        {
            var employee = this.dbContext.Employees.FirstOrDefault(e => e.Email == email);
            if(employee == null)
            {
                var customer = this.dbContext.Customers.FirstOrDefault(e => e.Email == email);
                if(customer == null)
                {
                    return null;
                }

                return Access.Customer;
            }

            return Access.Admin;
        }

        public bool GetEmail(string email)
        {
            if(this.Get(email) != null)
            {
                return true;
            }

            return false;
        }

        public string GetUserName(string email)
        {
            string userName = this.dbContext.Customers.Where(c => c.Email == email).Select(c => c.FirstName).FirstOrDefault();

            if(userName == null)
            {
                userName = this.dbContext.Employees.Where(c => c.Email == email).Select(c => c.FirstName).FirstOrDefault();
            }

            return userName;
        }
    }
}
