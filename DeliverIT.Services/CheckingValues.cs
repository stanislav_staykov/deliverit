﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data;
using DeliverIT.Data.Enum;
using DeliverIT.Services.Contracts;

namespace DeliverIT.Services
{
    public class CheckingValues : ICheckingValues
    {
        private readonly DeliverItContext dbContext;

        public CheckingValues(DeliverItContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public int CategoryCheck(string name)
        {
            try
            {
                var id = this.dbContext.Categories.Where(c => c.Name == name).Select(c => c.Id).First();
                return id;
            }
            catch(InvalidOperationException)
            {
                throw new ArgumentNullException("This Werehouse not exist.");
            }
        }

        public bool WerehouseCheck(int id)
        {
            if(this.dbContext.Warehouses.FirstOrDefault(w => w.Id == id) != null)
            {
                return true;
            }
            else
                return false;
        }

        public int FindIdViaEmail(string email)
        {
            try
            {
                var id = this.dbContext.Customers.Where(c => c.Email == email).Select(c => c.Id).First();
                return id;
            }
            catch(InvalidOperationException)
            {
                throw new ArgumentException("This Customer not exist.");
            }
        }

        public double WeightCheck(string weight)
        {
            double recult;
            if(Double.TryParse(weight, out recult))
            {
                return recult;
            }
            else 
            {
                throw new ArgumentNullException("Not correct value for weight!");
            }
        }

        public int WerehouseCheck(string Name)
        {
            try
            {
                var id = this.dbContext.Warehouses.Where(w => w.Name == Name).Select(w => w.Id).First();
                return id;
            }
            catch(InvalidOperationException)
            {
                throw new ArgumentException("This Werehouse not exist.");
            }           
        }

        public DateTime ValidateDateTime(string dateTime)
        {
            DateTime temp;
            if(DateTime.TryParse(dateTime, out temp))
            {
                return temp;
            }
            else
            {
                throw new ArgumentException("Not correct value for data time!");
            }
        }

        public Status CheckStatus(string status)
        {
            Status value;
            if(Status.TryParse(status, out value))
            {
                return value;
            }
            else
            {
                throw new ArgumentException("Not correct value for status!");
            }
        }

        public bool CheckWarehouse(string name)
        {
            if(name.Length > 2 && name.Length < 20)
            {
                return true;
            }
            return false;
        }
    }
}
