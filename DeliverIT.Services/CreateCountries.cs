﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services
{
    public class CreateCountries : ICreateCountries
    {
        private readonly DeliverItContext dbContext;

        public CreateCountries(DeliverItContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public int CreateCountry(ICountry countryData)
        {
            int id;
            if(countryData.Country == null)
            {
                throw new ArgumentNullException("Insert value for country name!");
            }
            var countryName = this.dbContext.Countries.FirstOrDefault(c => c.Name == countryData.Country);
            if(countryName != null)
            {
                id = countryName.Id;
            }
            else
            {
                Country country = new Country();
                country.Name = countryData.Country;
                this.dbContext.Countries.Add(country);
                this.dbContext.SaveChanges();
                id = this.dbContext.Countries.Where(c => c.Name == countryData.Country).Select(c => c.Id).First();
            }
            return id;
        }
    }
}
