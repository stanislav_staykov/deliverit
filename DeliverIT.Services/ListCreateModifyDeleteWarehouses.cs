﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Services
{
    public class ListCreateModifyDeleteWarehouses : IListCreateModifyDeleteWarehouses
    {
        private readonly DeliverItContext dbContext;
        private readonly ICheckingValues checkingValues;
        private readonly ICreateAddresses createAddresses;

        public ListCreateModifyDeleteWarehouses(DeliverItContext dbContext,
                                       ICheckingValues checkingValues,
                                       ICreateAddresses createAddresses)
        {
            this.dbContext = dbContext;
            this.checkingValues = checkingValues;
            this.createAddresses = createAddresses;
        }
        public List<WarehouseDTO> Listing()
        {
            List<WarehouseDTO> result = this.dbContext.Warehouses.Include(w => w.Address)
                                                           .ThenInclude(a => a.City)
                                                           .ThenInclude(c => c.Country)
                                                           .Select(p => new WarehouseDTO(p) { })
                                                           .ToList();

            return result;
        }

        public List<Warehouse> ListingWarehouse()
        {
            List<Warehouse> result = this.dbContext.Warehouses.Include(w => w.Address)
                                                           .ThenInclude(a => a.City)
                                                           .ThenInclude(c => c.Country)
                                                           .ToList();

            return result;
        }

        public Warehouse ListingWarehouseById(int id)
        {
            Warehouse result = this.dbContext.Warehouses.Include(w => w.Address)
                                                           .ThenInclude(a => a.City)
                                                           .ThenInclude(c => c.Country)
                                                           .Where(w => w.Id == id)
                                                           .First();

            return result;
        }

        public IEnumerable<Address> GetAllAddresses()
        {
            var addresses = this.dbContext.Addresses;

            return addresses;
        } 

        public WarehouseDTO Create(WarehouseDTO warehouseData)
        {
            var createWarehouse = new Warehouse();
            var result = this.dbContext.Warehouses.Where(w => w.Name == warehouseData.Warehouse).FirstOrDefault();
            if(result != null)
            {
                throw new ArgumentException("Warehouse with that name exist!");
            }
            createWarehouse.Name = this.checkingValues.CheckWarehouse(warehouseData.Warehouse) ? warehouseData.Warehouse : throw new ArgumentException("Value should be between 2 and 10 characters.");
            createWarehouse.AddressId = this.createAddresses.CreateAddress(warehouseData);            
            this.dbContext.Warehouses.Add(createWarehouse);
            this.dbContext.SaveChanges();
            WarehouseDTO warehouse = this.dbContext.Warehouses.Include(w => w.Address)
                                                           .ThenInclude(a => a.City)
                                                           .ThenInclude(c => c.Country)
                                                           .OrderByDescending(p => p.Id)
                                                           .Select(p => new WarehouseDTO(p) { })
                                                           .First();

            return warehouse;
        }
        public WarehouseDTO Modify(string name, string newName)
        {
            var warehouseModify = dbContext.Warehouses.FirstOrDefault(w => w.Name == name);
            if(warehouseModify != null)
            {
                warehouseModify.Name = newName;
                dbContext.SaveChanges();
                WarehouseDTO result = this.dbContext.Warehouses.Include(w => w.Address)
                                                           .ThenInclude(a => a.City)
                                                           .ThenInclude(c => c.Country)
                                                           .Where(w => w.Name == newName)
                                                           .Select(p => new WarehouseDTO(p) { })
                                                           .First();

                return result;
            }
            throw new ArgumentException("No parcel with that id.");
        }
    }
}
